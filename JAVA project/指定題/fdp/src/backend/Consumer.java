package backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.json.JSONArray;

import backend.Order.Status;

public class Consumer {
	public String name;
	public Boolean pay = false;
	private String password;
	public String phoneNumber;
	public String payDate;
	public double lat;
	public double lng;
	ArrayList<Order> orders = new ArrayList<Order>();

	public void save(Connection conn) {
		try {
			String sql = "INSERT INTO consumer (name, pay,password,phoneNumber,payDate) VALUES (?,?,?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, this.name);
			pstmt.setBoolean(2, this.pay);
			pstmt.setString(3, this.password);
			pstmt.setString(4, this.phoneNumber);
			pstmt.setString(5, this.payDate);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	// 下訂單，輸入訂單資訊，創造一個訂單並放進DataBase
	public Order order(Connection conn, int price, Store store, Order order) {
		java.sql.Date timeOfOrderCreated = new java.sql.Date((new Date()).getTime());
		SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddhhmmss");
//  order.orderID = ft.format(timeOfOrderCreated);
		order.price = price;
		order.timeOfOrderCreated = timeOfOrderCreated;
		order.deliveryLocation = "String.valueOf(lat)" + "String.valueOf(lng)";
		order.storename = store.name;
		order.orderStatus = Status.Confirming_order.toString();
		order.save(conn);
		return order;

//  double dis = Math.sqrt(Math.pow(lat - store.latitude, 2) + Math.pow(lng - store.longitude, 2));
//  order.divMoney = (int) (dis * 1000);
//  if (pay) {
//   order.divMoney = 0;
//  }
//  order.save(conn);
//  return order;
	}

	// 查看歷史訂單
	public ArrayList<Order> getHistoryOrder(Connection conn) throws SQLException {
		Statement statement = conn.createStatement();
		ResultSet rs = statement.executeQuery("select * from 'order'");

		ArrayList<Order> historyOrder = new ArrayList<Order>();
		while (rs.next()) {
			Order o = new Order();
			o.orderID = rs.getInt("orderID");
			o.orderStatus = rs.getString("orderStatus");
			o.price = rs.getDouble("price");
			o.timeOfOrderCreated = rs.getDate("timeOfOrderCreated");
			o.timeOfArrival = rs.getDate("timeOfArrival");
			o.deliveryLocation = rs.getString("deliveryLocation");
			o.consumerName = rs.getString("consumerName");
			o.deliveryPersonName = rs.getString("deliveryPersonName");
			o.storename = rs.getString("storename");
			o.mealsize = rs.getInt("mealsize");
			o.Info = rs.getString("Info");

			o.selectedItems = new String[o.mealsize];
			JSONArray si = new JSONArray(rs.getString("selectedItems"));
			for (int i = 0; i < si.length(); i++) {
				o.selectedItems[i] = si.getString(i);
			}
			o.selectedPrice = new String[o.mealsize];
			JSONArray sp = new JSONArray(rs.getString("selectedPrice"));
			for (int i = 0; i < sp.length(); i++) {
				o.selectedPrice[i] = sp.getString(i);
			}
			o.selectedNumber = new int[o.mealsize];
			JSONArray sn = new JSONArray(rs.getString("selectedNumber"));
			for (int i = 0; i < sn.length(); i++) {
				o.selectedNumber[i] = sn.getInt(i);
			}
			historyOrder.add(o);
		}
		return historyOrder;
	}

//  找最新的訂單
	public Order checkOrder(Connection conn) {

		try {
			ArrayList<Order> historyOrder = getHistoryOrder(conn);
			historyOrder.sort((o1, o2) -> o2.timeOfOrderCreated.compareTo(o1.timeOfOrderCreated));
			if (historyOrder.isEmpty()) {
				return null;
			}
			System.out.println(historyOrder.get(0).orderID);
			return historyOrder.get(0);
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

//  改變訂單狀態
// changeOrderStatus( 123 ,conn , Status.Confirming_order.toString());
	public void changeOrderStatus(int OrderID, Connection conn, String status) throws SQLException {
		String query = "UPDATE 'order' SET orderStatus=? WHERE orderID=?";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, status);
		statement.setInt(2, OrderID);
		statement.executeUpdate();
	}

	public ArrayList<Store> getAllStoresDB(Connection conn) throws Exception {
		Statement statement = conn.createStatement();
		ResultSet rs = statement.executeQuery("select * from store");

		ArrayList<Store> stores = new ArrayList<Store>();
		while (rs.next()) {
			Store s = new Store();
			s.name = rs.getString("name");
			s.address = rs.getString("address");
			stores.add(s);
		}
		return stores;
	}

	// 依名字找餐廳
	public Store getStoreByName(ArrayList<Store> storeDB, String name) throws Exception {
		for (Store store : storeDB) {
			if (store.name.equals(name)) {
				return store;
			}
		}
		throw new Exception("Not found");
	}

	public Store getStoreByNameDB(Connection conn, String name) throws Exception {
		ArrayList<Store> storeDB = getAllStoresDB(conn);
		return this.getStoreByName(storeDB, name);

	}

	// 依類別找餐廳
	public ArrayList<Store> getStoreByType(ArrayList<Store> storeDB, String type) throws Exception {
		ArrayList<Store> typeStores = new ArrayList<Store>();
		for (Store store : storeDB) {
			if (Arrays.asList(store.types).contains(type)) {
				typeStores.add(store);
			}
		}
		return typeStores;
	}
}