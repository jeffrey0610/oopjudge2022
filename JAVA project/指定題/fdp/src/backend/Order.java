package backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import org.json.JSONArray;

public class Order {
	// 等待商家確認、餐點製作中、餐點運送中、餐點抵達
	public int orderID;
	public String orderStatus;
	public double price = 0;
	public java.sql.Date timeOfOrderCreated;
	public java.sql.Date timeOfArrival;
	public String deliveryLocation;
	public String consumerName;
	public String deliveryPersonName;
	public storeInfoAll store;
	public String storename;
	public String Info;
	public String[] selectedItems = new String[1];
	public String[] selectedPrice = new String[1];
	public int[] selectedNumber = new int[1];
	public int mealsize;
	public static boolean thereIsOrder;
	public static Order myOrder;
	public static boolean courierConfirm = false;
	public static boolean courierReject = false;
	public static boolean storeConfirm = false;
	public static boolean storeReject = false;

	//	第幾項Store
	public int xStore;

	//Order class is used as a bridge among member, store and courier.
	//It helps avoid duplicated declarations.
	public Order() {
		myOrder = this;
	}
	// 	訂單狀態：等待商家確認、餐點製作中、餐點運送中、餐點抵達
	public enum Status {
		Confirming_order, Order_is_being_prepared, Order_is_on_the_way, Order_is_arrived , Order_failed
	}
	
	//  改變訂單狀態 EX.changeOrderStatus( 123 ,conn , Status.Confirming_order.toString());
	public void changeOrderStatus(int OrderID, Connection conn, String status) throws SQLException {
		String query = "UPDATE 'order' SET orderStatus=? WHERE orderID=?";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, status);
		statement.setInt(2, OrderID);
		statement.executeUpdate();
	}

	//	將Order的資訊放進SQLite
	public void save(Connection conn) {
		try {

			String sql = "INSERT OR REPLACE INTO 'order' (orderID, orderStatus,price,timeOfOrderCreated,timeOfArrival,deliveryLocation,consumerName,deliveryPersonName,storename,mealsize,Info,selectedItems,selectedPrice,selectedNumber) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			if (this.orderID != 0) {
				pstmt.setInt(1, this.orderID);
			}
			pstmt.setString(2, this.orderStatus.toString());
			pstmt.setDouble(3, this.price);
			pstmt.setDate(4, this.timeOfOrderCreated);
			pstmt.setDate(5, this.timeOfArrival);
			pstmt.setString(6, this.deliveryLocation);
			pstmt.setString(7, this.consumerName);
			pstmt.setString(8, this.deliveryPersonName);
			pstmt.setString(9, this.storename);
			pstmt.setInt(10, this.mealsize);
			pstmt.setString(11, this.Info);
			JSONArray si = new JSONArray(this.selectedItems);
			pstmt.setString(12, si.toString());
			JSONArray sp = new JSONArray(this.selectedPrice);
			pstmt.setString(13, sp.toString());
			JSONArray sn = new JSONArray(this.selectedNumber);
			pstmt.setString(14, sn.toString());

			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}