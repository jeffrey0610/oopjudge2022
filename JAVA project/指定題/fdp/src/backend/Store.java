package backend;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.*;
import org.json.simple.parser.ParseException;

import backend.Order.Status;

public class Store {
	public String name;
	public String account;
	public String password;
	public String address;
	public String email;
	public String latitude;
	public String longitude;
	public String phone;
	public String store_description;
	public String order_description;
	public String html;
	public String[] types;
	public String[] menuName;
	public String[] menuPrice;
	public String[][] businesstime = new String[7][2];
	public ArrayList<Order> orders = new ArrayList<Order>();
	// 是否接單
	boolean acceptOrder;
	// 需滿額折扣價格
	int discountPrice;
	// 折扣價格
	int discount;

	public Store() {
	}

	// 建立商家資訊
	public Store(String storename) {
		storeInfoAll stores = null;
		try {
			stores = new storeInfoAll();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < stores.getName().length; i++) {
			if (storename.equals(stores.getName(i))) {
				this.name = storename;
				this.address = stores.getAddress(i);
				this.latitude = stores.getLatitude(i);
				this.longitude = stores.getLongitude(i);
				this.phone = stores.getPhone(i);
				this.store_description = stores.getStoreDescription(i);
				this.order_description = stores.getOrderDescription(i);
				this.types = stores.getType(i);
//    this.menuName = stores.getMenuName(i);
//    this.menuPrice = stores.getMenuPrice(i);

				for (int k = 0; k < 7; k++) {
					for (int j = 0; j < 2; j++) {
						this.businesstime[k][j] = stores.getBusinessTime(i, k, j);
					}
				}
			}
		}
	}

	//  改變訂單狀態 EX.changeOrderStatus( 123 ,conn , Status.Confirming_order.toString());
	public void changeOrderStatus(int OrderID, Connection conn, String status) throws SQLException {
		String query = "UPDATE 'order' SET orderStatus=? WHERE orderID=?";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, status);
		statement.setInt(2, OrderID);
		statement.executeUpdate();
	}

	// 查看商品價錢
	public int getPrice(String food) {
		for (int i = 0; i < menuName.length; i++) {
			if (food.equals(menuName[i])) {
				return Integer.parseInt(menuPrice[i]);
			}
		}
		return 0;
	}

	// 查看歷史訂單
	public ArrayList<Order> getHistoryOrder(Connection conn) throws SQLException {
		String query = "select * from 'order' where storename=?";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, this.name);
		ResultSet rs = statement.executeQuery();

		ArrayList<Order> historyOrder = new ArrayList<Order>();
		while (rs.next()) {

			Order o = new Order();
			o.orderID = rs.getInt("orderID");
			o.orderStatus = rs.getString("orderStatus");
			o.price = rs.getDouble("price");
			o.timeOfOrderCreated = rs.getDate("timeOfOrderCreated");
			o.timeOfArrival = rs.getDate("timeOfArrival");
			o.deliveryLocation = rs.getString("deliveryLocation");
			o.consumerName = rs.getString("consumerName");
			o.deliveryPersonName = rs.getString("deliveryPersonName");
			o.storename = rs.getString("storename");
			o.Info = rs.getString("Info");

			historyOrder.add(o);
		}
		return historyOrder;
	}

	// 是否為營業時間
	public boolean isOpen() throws java.text.ParseException {
		Date now = new Date();
		SimpleDateFormat getWeek = new SimpleDateFormat("EEEE");
		SimpleDateFormat getTime = new SimpleDateFormat("hh:mm");
		String nowString = getTime.format(now);
		Date nowTime = getTime.parse(nowString);
		Date openTime = getTime.parse(this.businesstime[0][0]);
		Date closeTime = getTime.parse(this.businesstime[0][1]);
		switch (getWeek.format(now)) {
		case "星期一":
			if (nowTime.compareTo(openTime) > 0 && nowTime.compareTo(closeTime) < 0) {
				return true;
			}
		case "星期二":
			if (nowTime.compareTo(openTime) > 0 && nowTime.compareTo(closeTime) < 0) {
				return true;
			}
		case "星期三":
			if (nowTime.compareTo(openTime) > 0 && nowTime.compareTo(closeTime) < 0) {
				return true;
			}
		case "星期四":
			if (nowTime.compareTo(openTime) > 0 && nowTime.compareTo(closeTime) < 0) {
				return true;
			}
		case "星期五":
			if (nowTime.compareTo(openTime) > 0 && nowTime.compareTo(closeTime) < 0) {
				return true;
			}
		case "星期六":
			if (nowTime.compareTo(openTime) > 0 && nowTime.compareTo(closeTime) < 0) {
				return true;
			}
		case "星期日":
			if (nowTime.compareTo(openTime) > 0 && nowTime.compareTo(closeTime) < 0) {
				return true;
			}
		default:
			return false;
		}
	}

	// 是否有折扣
	public boolean haveDiscount(Order order) {
		if (order.price >= this.discountPrice) {
			return true;
		}
		return false;
	}

}