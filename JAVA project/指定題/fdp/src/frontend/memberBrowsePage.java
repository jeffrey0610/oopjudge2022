package frontend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

import backend.Courier;
import backend.categoryInfo;
import backend.storeInfoAll;

public class memberBrowsePage extends MemberHomePage{
	
	public static final int LINES = 26;
	public static final int CHAR_PER_LINE = 80;
	public JButton[] testttButton;
	public JPanel memberHomePanel;
	public storeInfoAll store;
	public categoryInfo categoryData;
	public JButton[] CategoryButton;
	public goToOrderListener goToOrderNextListener;
	public goToTypeSearchListener goToTypeSearchNextListener;
	public goToTimeSearchListener goToTimeSearchNextListener;
	public JButton[] TimeButton;
	
	public memberBrowsePage(int a)
	{
		try {
			initialization();
			CategoryButtons(a);
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public memberBrowsePage(int a, double b, double c)
	{
		try {
			initialization();
			TimeButtons(a, b, c);
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Search by time directory
	private void TimeButtons(int a, double b, double c) {
		
		//default time required for stores to prepare order is set to 10 minutes. 
		int time = a;
		int preparingTime = 10;
		goToOrderNextListener = new goToOrderListener();
		goToTimeSearchNextListener = new goToTimeSearchListener();
		memberHomePanel = new JPanel();
		memberHomePanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberHomePanel);
		
		store = null;
		try {
			store = new storeInfoAll();
		} catch (IOException | ParseException eee) {
			// TODO Auto-generated catch block
			eee.printStackTrace();
		}
		Courier courier = new Courier();
		TimeButton = new JButton[store.getName().length];
		
		//calculate the time and categorize stores by time
		for(int i = 0; i < store.getName().length; i++)
		{
			int dis = (int) courier.getDistance(Double.parseDouble(store.getLatitude(i)), Double.parseDouble(store.getLongitude(i)), b, c);
			int TimeTake = dis/1250;
			int totalTime = TimeTake + preparingTime;
			if(totalTime < time && totalTime > time - 10)
			{
				TimeButton[i] = new JButton(store.getName(i).toString());
				TimeButton[i].addActionListener(goToOrderNextListener);
				memberHomePanel.add(TimeButton[i]);
				TimeButton[i].setBackground(Color.PINK);
			}
		}
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(goToTimeSearchNextListener);
		memberHomePanel.add(backButton);
	}

	//Search by type directory
	private void CategoryButtons(int a) {
		
		categoryData = null;
		store = null;
		
		try {
			categoryData = new categoryInfo();
			store = new storeInfoAll();
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		goToOrderNextListener = new goToOrderListener();
		goToTypeSearchNextListener = new goToTypeSearchListener(); 
		int[] typesize = new int[store.getName().length];
		try {
			typesize = store.getTypeSize();
		} catch (IOException | ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		memberHomePanel = new JPanel();
		memberHomePanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberHomePanel);
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(goToTypeSearchNextListener);
		memberHomePanel.add(backButton);
		this.CategoryButton = new JButton[store.getName().length];
		
		for(int i = 0; i < store.getName().length; i++)
		{
			for(int j = 0; j < typesize[i]; j++)
			{
				if(store.getType(i, j).equals(categoryData.getCategory(a)))
				{
					CategoryButton[i] = new JButton(store.getName(i).toString());
					CategoryButton[i].addActionListener(goToOrderNextListener);
					memberHomePanel.add(CategoryButton[i]);
					CategoryButton[i].setBackground(Color.PINK);
				}
			}
		}
		
	}

	public memberBrowsePage()
	{
		try {
			initialization();
			ButtonSet();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//Browse all stores buttons setup
	private void ButtonSet() {
		goToOrderNextListener = new goToOrderListener();
		
		memberHomePanel = new JPanel();
		memberHomePanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberHomePanel);
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(goToOrderNextListener);
		memberHomePanel.add(backButton);
		
		store = null;
		try {
			store = new storeInfoAll();
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.testttButton = new JButton[store.getName().length];
		
		for(int i = 0; i < store.getName().length ; i++)
		{
			testttButton[i] = new JButton(store.getName(i).toString());
			testttButton[i].addActionListener(goToOrderNextListener);
			memberHomePanel.add(testttButton[i]);
			testttButton[i].setBackground(Color.PINK);
		}
	}

	public void initialization() throws FileNotFoundException, IOException, ParseException
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class goToOrderListener implements ActionListener {
	
		//go to the OrderPage of the selected store
		public void actionPerformed(ActionEvent e) {
			
			memberBrowsePage.frame.dispose();
			String actioncommand = e.getActionCommand();
			store = null;
			try {
				store = new storeInfoAll();
			} catch (IOException | ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			for(int i = 0; i < store.getName().length; i++)
			{
				if(actioncommand.equals(store.getName(i).toString()))
				{
					OrderPage order = new OrderPage(i);
					order.frame.setVisible(true);
					break;
				}
			}
			
			if(actioncommand.equals("Back"))
			{
				MemberHomePage HomePage = new MemberHomePage(memberTrack);
				HomePage.frame.setVisible(true);
			}
		}

	}
	
	//go back to Search by Name Page
	public class goToTypeSearchListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {	
			memberBrowsePage.frame.dispose();
			String actioncommand = e.getActionCommand();
			if(actioncommand.equals("Back"))
			{
				memberSearchPage TypeSearchPage = new memberSearchPage(1, 1);
				TypeSearchPage.frame.setVisible(true);
			}
		}
		
	}
	
	//Go back to search by time page
	public class goToTimeSearchListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			memberBrowsePage.frame.dispose();
			String actioncommand = e.getActionCommand();
			if(actioncommand.equals("Back"))
			{
				memberSearchPage TimeSearchPage = new memberSearchPage(1, 1, 1);
				TimeSearchPage.frame.setVisible(true);
			}
		}
		
	}
}

