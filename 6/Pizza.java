/**
 * Pizza
 * @author jeffr
 *
 */
public class Pizza {
	/**
	 * @param size
	 * Pizza的大小 有large medium small三種
	 * @param cheese
	 * Pizza 上有幾份cheese
	 * @param pepperoni
	 * Pizza 上有幾份pepperoni
	 * @param ham
	 * Pizza 上有幾份ham
	 */
	private String size;
	public void setSize(String size) {
		this.size = size;
	}
	public String getSize() {
		return size;
	}

	private int cheese;
	public int getNumberOfCheese() {
		return cheese;
	}
	public void setNumberOfCheese(int cheese) {
		this.cheese = cheese;
	}
	
	private int pepperoni;
	
	public int getNumberOfPepperoni() {
		return pepperoni;
	}
	public void setNumberOfPepperoni(int pepperoni) {
		this.pepperoni = pepperoni;
	}

	private int ham;
	
	public int getNumberOfHam() {
		return ham;
	}
	public void setNumberOfHam(int ham) {
		this.ham = ham;
	}
	
	public Pizza(String size,int cheese,int pepperoni,int ham) {
		this.size = size;
		this.cheese = cheese;
		this.pepperoni = pepperoni;
		this.ham = ham;
	}
	/**
	 * 預設Pizza的
	 * size:small
	 * cheese=1
	 * pepperoni=1
	 * ham=1
	 * 
	 */
	public Pizza() {
		size="small";
		cheese=1;
		pepperoni=1;
		ham=1;
	}
	/**
	 * 每份料2塊
	 * 如果是large的 +14塊
	 * 如果是medium的 +12塊
	 * 如果是small大的 +10塊
	 * 
	 * @return 2*(cheese+pepperoni+ham)+cost of size
	 */
	public double calcCost() {
		switch(this.size) {
		case "small": 
			return 10+2.0*(cheese+pepperoni+ham);
		
		case "medium":
			return 12+2.0*(cheese+pepperoni+ham);
			
		case "large":
			return 14+2.0*(cheese+pepperoni+ham);
			
		default:
			return 0.0;	
		}
	}
	/**
	 * 看另一個pizza跟這個pizza是不是一模一樣
	 * @param Pizza 另一個pizza
	 * @return 看另一個pizza跟這個pizza是不是一模一樣
	 */
	public boolean equals(Pizza Pizza) {
		if(this.size==Pizza.size && this.cheese==Pizza.cheese && this.pepperoni==Pizza.pepperoni && this.ham == Pizza.ham)
			return true;
		else
			return false;
	}
	/**
	 * Pizza的詳細資料
	 * @return Pizza的詳細資料
	 */
	public String toString() {
		return("size = " +this.size+ ", numOfCheese = " +this.cheese+ ", numOfPepperoni = " +this.pepperoni+ ", numOfHam = " +this.ham);
	}
}
