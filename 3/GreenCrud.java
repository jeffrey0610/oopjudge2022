
/**
 * 計算 Green Crud的population
 * @author 高孝傑
 *
 */
public class GreenCrud {
	/**
	 * 
	 * 先將days除以5 看他有幾個5天一循環的週期
	 * 在看他是Fibonacci sequence的第幾項
	 * 乘上a後即是答案了
	 * 
	 * @param initialSize
	 * 最初起始值 
	 * @param days
	 * 經歷過幾天
	 * @return
	 * initialSize*(Fibonacci的第days/5項)
	 */
	public static int calPopulation(int initialSize, int days) {
		if( initialSize < 1 || initialSize>10000 || days<1 || days>100) {
			return 0;
		}
		days = days / 5;
		/**
		 * Fibonacci sequence
		 */
		int a=1;
		int b=1;
		int c=1;
		for(int i=0 ; i<days ; i++) {
			if(i==0) {
				c=1;
			}else {
				c=b+a;
				a=b;
				b=c;
			}
		}return c*initialSize;
	}
}
