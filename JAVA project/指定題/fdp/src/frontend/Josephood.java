package frontend;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

import backend.Consumer;
import backend.JSONUtils;
import backend.Order;
import backend.Store;
import backend.storeInfoAll;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class Josephood {
	//Josephood is a food blog in reality.
	//Joseph + food = Josephood, a well known chef in Taiwan. 
	//create Consumer "me", any class who extends Josephood can simply use this.
	Consumer me = new Consumer();
	SpringLayout springLayout = new SpringLayout();
	public Container FB = null;
	public static Connection conn = null;
	public static ArrayList<Order> orders = new ArrayList<Order>();
	public static ArrayList<Store> stores = new ArrayList<Store>();

	// create SQLite Database
		public static Connection createNewTable() throws Exception {
			// SQLite connection string
			String url = "jdbc:sqlite:./assets/java-sqlite.db";

			try {
				Connection conn = DriverManager.getConnection(url);
				Statement stmt = conn.createStatement();

				// SQL statement for creating a new table store
				stmt.executeUpdate("drop table if exists store");
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS store (\n" + " name text PRIMARY KEY, \n"
						+ " account text , \n" + " password text , \n" + " email text , \n" + " address text, \n"
						+ " latitude text, \n" + " longitude text, \n" + " phone text, \n" + " discount text, \n"
						+ " store_description text, \n" + " html \n" + ");");
				// SQL statement for creating a new table courier
				stmt.executeUpdate("drop table if exists courier");
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS courier (\n" + " name text PRIMARY KEY, \n"
						+ " account text, \n" + " password text, \n" + " phoneNumber text, \n" + " email text \n" + ");");
				// SQL statement for creating a new table consumer
				stmt.executeUpdate("drop table if exists consumer");
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS consumer (\n" + " name text PRIMARY KEY,\n"
						+ " account text, \n" + " password text, \n" + " phonenumber text, \n" + " email text, \n"
						+ " pay integer, \n" + " payDate text \n" + ");");
				// SQL statement for creating a new table order
				stmt.executeUpdate("drop table if exists 'order'");
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS 'order' (\n"
						+ " orderID integer PRIMARY KEY AUTOINCREMENT,\n" + " orderStatus text, \n" + " price text, \n"
						+ " timeOfOrderCreated Date, \n" + " timeOfArrival Date, \n" + " deliveryLocation text ,\n"
						+ " consumerName text, \n" + " deliveryPersonName text, \n" + " storename text, \n"
						+ " mealsize integer, \n" + " selectedItems text, \n" + " selectedPrice text, \n"
						+ " selectedNumber text, \n" + " Info text \n" + ");");
				// @formatter:on
				System.out.println("Create table finished.");
				return conn;

			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			throw new Exception("db error");
		}

	// Import Stores from JSON
	public static void importStores(Connection conn) throws FileNotFoundException, IOException, ParseException {
		JSONArray jsonArray = JSONUtils.getJSONArrayFromFile("/stores_detail.json");
		storeInfoAll storeInfoAll = new storeInfoAll();
		for (int i = 0; i < storeInfoAll.getName().length; i++) {
			JSONObject obj = jsonArray.getJSONObject(i);
			Store s = new Store();
			s.name = storeInfoAll.getName(i);
			s.account = storeInfoAll.getName(i);
			s.password = "00000000";
//			s.email =
			s.address = storeInfoAll.getAddress(i);
			s.latitude = storeInfoAll.getLatitude(i);
			s.longitude = storeInfoAll.getLongitude(i);
			s.phone = storeInfoAll.getPhone(i);
//			s.discount = storeInfoAll.
			s.store_description = storeInfoAll.getStoreDescription(i);
			s.html = storeInfoAll.googleAPI(i);
			stores.add(s);

			try {
				String sql = "INSERT INTO store (name,account,password,address,latitude,longitude,phone,store_description,html) VALUES (?,?,?,?,?,?,?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, s.name);
				pstmt.setString(2, s.account);
				pstmt.setString(3, s.password);
				pstmt.setString(4, s.address);
				pstmt.setString(5, s.latitude);
				pstmt.setString(6, s.longitude);
				pstmt.setString(7, s.phone);
				pstmt.setString(8, s.store_description);
				pstmt.setString(9, s.html);
				pstmt.executeUpdate();

			} catch (Exception e) {
				System.out.println(e);
			}

		}
	}
//-------------------------------------------------------------------------------------------------------------------------------- 
//Interface
	
	public static final int WIDTH = 1000;
	public static final int HEIGHT = 600;

	static JFrame frame;

	//main class
	public static void main(String[] args) {
		try {
			conn = createNewTable();
			importStores(conn);
		} catch (Exception e) {
			System.out.println(e);

		}

		Josephood HomePage = new Josephood();
		HomePage.frame.setVisible(true);
	}

	public Josephood() {
		try {
			initialization();
			storeInfoAll store = new storeInfoAll();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//start page
	public void initialization() throws FileNotFoundException, IOException, ParseException {
		
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);// to put the frame in the middle of the screen
		HomePageListener HomePageNextListener = new HomePageListener();

		JPanel homePanel = new JPanel();
		homePanel.setBackground(new Color(70, 70, 70));
		frame.add(homePanel);
		homePanel.setLayout(springLayout);
		Container FB = frame.getContentPane();
		int dis = 250;

		JButton memberLoginButton = new JButton("Member");
		memberLoginButton.addActionListener(HomePageNextListener);
		homePanel.add(memberLoginButton);
		springLayout.putConstraint(SpringLayout.WEST, memberLoginButton, dis, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, memberLoginButton, 410, SpringLayout.NORTH, FB);

		JButton storeLoginButton = new JButton("Store");
		storeLoginButton.addActionListener(HomePageNextListener);
		homePanel.add(storeLoginButton);
		springLayout.putConstraint(SpringLayout.WEST, storeLoginButton, dis + 200, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeLoginButton, 410, SpringLayout.NORTH, FB);

		JButton courierLoginButton = new JButton("Courier");
		courierLoginButton.addActionListener(HomePageNextListener);
		homePanel.add(courierLoginButton);
		springLayout.putConstraint(SpringLayout.WEST, courierLoginButton, dis + 385, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, courierLoginButton, 410, SpringLayout.NORTH, FB);

		JLabel Welcometop = new JLabel("Josephood");
		homePanel.add(Welcometop);
		Welcometop.setFont(new Font("Serif", Font.BOLD, 160));
		Welcometop.setForeground(new Color(241, 250, 77));
		springLayout.putConstraint(SpringLayout.WEST, Welcometop, 128, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, Welcometop, 100, SpringLayout.NORTH, FB);

		JLabel Welcome = new JLabel("Josephood");
		homePanel.add(Welcome);
		Welcome.setFont(new Font("Serif", Font.BOLD, 160));
		Welcome.setForeground(new Color(176, 176, 169));
		springLayout.putConstraint(SpringLayout.WEST, Welcome, 132, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, Welcome, 104, SpringLayout.NORTH, FB);

		JLabel food = new JLabel("It's a sin to glutton, but a bless to savour");
		homePanel.add(food);
		food.setFont(new Font("Serif", Font.CENTER_BASELINE, 25));
		food.setForeground(new Color(206, 207, 130));
		springLayout.putConstraint(SpringLayout.WEST, food, 275, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, food, 320, SpringLayout.NORTH, FB);

	}

	public class HomePageListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			Josephood.frame.dispose();

			String actioncommand = e.getActionCommand();

			if (actioncommand.equals("Member")) {
				MemberLoginPage MHP = new MemberLoginPage();
				MHP.frame.setVisible(true);
			} else if (actioncommand.equals("Store")) {
				StoreLoginPage SLP = new StoreLoginPage();
				SLP.frame.setVisible(true);
			} else if (actioncommand.equals("Courier")) {
				CourierLoginPage CHP = new CourierLoginPage();
				CHP.frame.setVisible(true);
			} else if (actioncommand.equals("Back")) {
				Josephood home = new Josephood();
				home.frame.setVisible(true);
			}
		}

	}

}