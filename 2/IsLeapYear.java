
public class IsLeapYear {
	/**
	 *  @param year
	 * 
	 * 高孝傑
	 * 設輸入值為 year
	 * 
	 * 如果year不在 1~10000 之間
	 * 則回傳 false
	 * 
	 * 如果year被4整除且不被100整除 
	 * 或是 year 被400整除
	 * 則回傳 true
	 * 
	 * 若不符合上述條件
	 * 則回傳 false
	 * 
	 * 
	 * @return year是否為閏年
	 */
	public static boolean determine(int year) {
		if( year < 1 || year>10000) {
			return false;
		}
		
		else if( year%4==0 && year%100!=0 || year%400==0 ) {
			return true;
		}
		else {
			return false;
		}
	}
}