
/**
 * ShapeFactory class主要是用來儲存所有Type的Shape
 * @author jeffr
 *
 */
public class ShapeFactory {
	/**
	 * 儲存所有Type of Shape，分別是
	    Triangle,
		Square, 
		Circle
	 * @author jeffr
	 *
	 */
	public enum Type{
		Triangle,
		Square, 
		Circle; 
	}
	/**
	 * 創造其中某一種ShapeFactory.Type的Object
	 * @param shapeType
	 * 某一種ShapeFactory.Type
	 * @param length
	 * ShapeFactory.Type的邊長或直徑
	 * @return
	 * 回傳一個創造出來的ShapeFactory.Type Object
	 */
	public Shape createShape(ShapeFactory.Type shapeType, double length) {
		if(shapeType.equals(ShapeFactory.Type.Triangle)){
			Shape Triangle = new Triangle(length);
	        return Triangle;
	    	} 
		else if(shapeType.equals(ShapeFactory.Type.Square)){
			Shape Square = new Square(length);
	        return Square;
			} 
		else if(shapeType.equals(ShapeFactory.Type.Circle)){
			Shape Circle = new Circle(length);
			return Circle;
	    	}
		return null;	
		}
	}

