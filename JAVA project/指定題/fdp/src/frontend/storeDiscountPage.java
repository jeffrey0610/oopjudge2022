package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

import backend.storeInfoAll;

public class storeDiscountPage extends StoreHomePage {

	public storeInfoAll store;
	
	public storeDiscountPage(int xStore)
	{
		initialization();
		showDiscount(xStore);
	}
	
	private void showDiscount(int xStore) {
		// TODO Auto-generated method stub
		StoreDiscountPageListener StoreDiscountPageNextListener = new StoreDiscountPageListener();
		store = null;
		try {
			store = new storeInfoAll();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JPanel storeHomePanel = new JPanel();
		storeHomePanel.setBackground(new Color(70, 70 , 70));
		frame.add(storeHomePanel);
		storeHomePanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(StoreDiscountPageNextListener);
		storeHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
		JLabel showDiscount = new JLabel(store.getOrderDescription(xStore) + store.getStoreDescription(xStore));
		storeHomePanel.add(showDiscount);
		showDiscount.setForeground(Color.WHITE);
		showDiscount.setFont(new Font("Serif", Font.PLAIN, 18));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, showDiscount, -5, SpringLayout.HORIZONTAL_CENTER,storeHomePanel);
		springLayout.putConstraint(SpringLayout.NORTH, showDiscount, 220, SpringLayout.NORTH, FB);
		
	}

	public storeDiscountPage()
	{
		initialization();
	}

	public void initialization()
	{
		springLayout = new SpringLayout();	
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class StoreDiscountPageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			storeDiscountPage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				StoreHomePage HomePage = new StoreHomePage(xStore);
				HomePage.frame.setVisible(true);
			}
			
		}

	}
}
