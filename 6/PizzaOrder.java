/**
 * Pizza的點餐系統 總共1~3個Pizza 且計算總價錢
 * @author jeffr
 *
 */
public class PizzaOrder {
	
	private int numberPizzas;
	public Pizza pizza1,pizza2,pizza3;
	/**
	 * 總共訂多少Pizza
	 * @param numberPizzas
	 * 總共訂多少Pizza
	 * @return
	 * 若numberPizzas不在1~3之間 則回傳false
	 */
	public boolean setNumberPizzas(int numberPizzas){
		if(numberPizzas>3 || numberPizzas<1) return false;
		else {
			this.numberPizzas=numberPizzas;
			return true;
		}
	}
	/**
	 * 點第一個Pizza
	 * @param pizza1
	 * 
	 */
	public void setPizza1(Pizza pizza1) {
		this.pizza1=pizza1;
	}
	/**
	 * 點第二個Pizza
	 * @param pizza2
	 * 第二個Pizza
	 * @return
	 * 若numberPizza不是2或3 回傳false
	 */
	public boolean setPizza2(Pizza pizza2) {
		if(numberPizzas==1) return false;
		else {
			this.pizza2=pizza2;
			return true;
		}
	}
	/**
	 * 點第三個Pizza
	 * @param pizza3
	 * 第三個Pizza
	 * @return
	 * 若numberPizza不是3 回傳false
	 */
	public boolean setPizza3(Pizza pizza3) {
		if(numberPizzas==3) {
			this.pizza3=pizza3;
			return true;
		}
		else return false;
	}
	/**
	 * 利用class Pizza的calcCost()
	 * 算出所有Pizza的金額
	 * @return
	 * 應付金額
	 */
	public double calcTotal() {
		switch(this.numberPizzas) {
		case 1:
			return pizza1.calcCost();
		case 2:
			return pizza1.calcCost()+pizza2.calcCost();
		case 3:
			return pizza1.calcCost()+pizza2.calcCost()+pizza3.calcCost();
		default:
			return 0.0;
		}	
	}
}
