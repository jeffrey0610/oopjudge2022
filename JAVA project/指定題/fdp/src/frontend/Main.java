package frontend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import backend.Consumer;
import backend.JSONUtils;
import backend.Order;
import backend.Store;
import backend.Order.Status;

public class Main {

	public static Connection conn = null;
	public static ArrayList<Order> orders = new ArrayList<Order>();
	public static ArrayList<Store> stores = new ArrayList<Store>();

	public static Connection createNewTable() throws Exception {
		// SQLite connection string
		String url = "jdbc:sqlite:./assets/java-sqlite.db";

		try {
			Connection conn = DriverManager.getConnection(url);
			Statement stmt = conn.createStatement();

			// SQL statement for creating a new table
			// @formatter:off
			stmt.executeUpdate("drop table if exists store");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS store (\n"
					+ " name text PRIMARY KEY, \n"
					+ " account text , \n"
					+ " password text , \n"
					+ " email text , \n"
					+ " address text, \n" 
					+ " latitude text, \n" 
					+ " longitude text, \n" 
					+ " phone text, \n" 
					+ " discount text, \n"
					+ " store_description text\n" 
					+ ");");
			
			stmt.executeUpdate("drop table if exists courier");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS courier (\n"
					+ " name text PRIMARY KEY, \n"
					+ " account text, \n" 
					+ " password text, \n" 
					+ " phoneNumber integer, \n" 
					+ " email text \n" 
					+ ");");
			
			stmt.executeUpdate("drop table if exists consumer");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS consumer (\n"
					+ " name text PRIMARY KEY,\n"
					+ " password text, \n" 
					+ " account String, \n" 
					+ " phonenumber String, \n" 
					+ " email String, \n" 
					+ " pay integer, \n" 
					+ " payDate text \n" 
					+ ");");
			
			stmt.executeUpdate("drop table if exists 'order'");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS 'order' (\n"
					+ " orderID integer PRIMARY KEY AUTOINCREMENT,\n"
					+ " orderStatus text, \n" 
					+ " price text, \n" 
					+ " timeOfOrderCreated Date, \n" 
					+ " timeOfArrival Date, \n" 
					+ " deliveryLocation text ,\n"
					+ " consumerName text, \n" 
					+ " deliveryPersonName text, \n" 
					+ " storename text, \n" 
					+ " mealsize integer, \n" 
					+ " selectedItems text, \n" 
					+ " selectedPrice text, \n" 
					+ " selectedNumber text, \n" 
					+ " Info text \n"
					+ ");");
			// @formatter:on
			System.out.println("Create table finished.");
			return conn;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		throw new Exception("db error");
	}

	public static void importStores(Connection conn) {
		JSONArray jsonArray = JSONUtils.getJSONArrayFromFile("/stores_detail.json");

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject obj = jsonArray.getJSONObject(i);
			Store s = new Store();
			for (String key : JSONObject.getNames(obj)) {
				if (key.equals("name")) {
					s.name = obj.getString(key);

				}

			}

			stores.add(s);

			try {
				String sql = "INSERT INTO store (name, address) VALUES (?,?)";

				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, s.name);
				pstmt.setString(2, s.address);
				pstmt.executeUpdate();

			} catch (Exception e) {
				System.out.println(e);
			}

		}

	}

	public static void main(String[] args) {

		try {
			conn = createNewTable();
			importStores(conn);
		} catch (Exception e) {
			System.out.println(e);

		}

		Consumer c = new Consumer();
		c.name = "jeffrey";
		c.save(conn);

		try {
			Store s = new Store("雞の專家_劍潭");
			Consumer f = new Consumer();
			Consumer f2 = new Consumer();
			Order order = new Order();
			f.order(conn, 200, s, order);
			f2.order(conn, 100, s, order);
			System.out.println(f.getHistoryOrder(conn).size());
			System.out.println(f.checkOrder(conn).orderID);
			System.out.println(s.getHistoryOrder(conn).size());
			
			System.out.println(c.getStoreByName(stores, "雞の專家_劍潭").name);
			System.out.println(c.getStoreByNameDB(conn, "雞の專家_劍潭").name);
			f.changeOrderStatus( 1 ,conn , Status.Order_is_being_prepared.toString());
		} catch (Exception e) {
			System.out.println(e);
		}

//		JSONObject obj = JSONUtils.getJsonObjectFromFile("/stores_detail.json");
//		String[] names = JSONObject.getNames(obj);
//		for(String key : names) {
//			System.out.println(key + ": " + obj.get(key));
//			Store s = new Store()
//					
//			if (key == "name") {
//				s.name = obj.get(key)				
//			}
//		}

	}

}
