package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

import backend.storeInfoAll;

public class storeProfilePage extends StoreHomePage {

	public JLabel[] item;
	public JLabel[] itemPrice;
	public JLabel StoreName;
	public JLabel StoreAddress;
	public JLabel StorePhone;
	public static int[] numberOfItems;
	public storeInfoAll store;
	public static int[] menuSize;
	public String[] day;
	public JLabel[] openDay;
	public JLabel orderDescription;
	
	public storeProfilePage()
	{
		initialization();
	}

	public storeProfilePage(int xstore)
	{
		initialization();
		StoreInfo(xstore);
	}
	
	private void StoreInfo(int myStore) {
		// TODO Auto-generated method stub
		StoreProfilePageListener StoreProfilePageNextListener = new StoreProfilePageListener();

		JPanel storeProfilePanel = new JPanel();
		storeProfilePanel.setBackground(new Color(70, 70 , 70));
		frame.add(storeProfilePanel);
		storeProfilePanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		try {
			store = new storeInfoAll();
			menuSize = store.getMenuSize();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//buttons
		JButton backButton = new JButton("Back");
		backButton.addActionListener(StoreProfilePageNextListener);
		storeProfilePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
		JButton historyButton = new JButton("History");
		historyButton.addActionListener(StoreProfilePageNextListener);
		storeProfilePanel.add(historyButton);
		springLayout.putConstraint(SpringLayout.WEST, historyButton, 867, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, historyButton, 450, SpringLayout.NORTH, FB);
		
		//profile info
		StoreName = new JLabel(store.getName(myStore));
		storeProfilePanel.add(StoreName);
		StoreName.setForeground(Color.WHITE);
		StoreName.setFont(new Font("Serif", Font.CENTER_BASELINE, 28));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, StoreName, 0, SpringLayout.HORIZONTAL_CENTER, storeProfilePanel);
		springLayout.putConstraint(SpringLayout.NORTH, StoreName, 17, SpringLayout.NORTH, FB);
		numberOfItems = new int[menuSize[myStore]];
		
		StoreAddress = new JLabel(store.getAddress(myStore));
		storeProfilePanel.add(StoreAddress);
		StoreAddress.setForeground(new Color(235, 235, 235));
		StoreAddress.setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, StoreAddress, 0, SpringLayout.HORIZONTAL_CENTER, storeProfilePanel);
		springLayout.putConstraint(SpringLayout.NORTH, StoreAddress, 80, SpringLayout.NORTH, FB);
		
		StorePhone = new JLabel(store.getPhone(myStore));
		storeProfilePanel.add(StorePhone);
		StorePhone.setForeground(new Color(235, 235, 235));
		StorePhone.setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, StorePhone, 0, SpringLayout.HORIZONTAL_CENTER, storeProfilePanel);
		springLayout.putConstraint(SpringLayout.NORTH, StorePhone, 120, SpringLayout.NORTH, FB);
		
		int BorderGap = 70;
		int BorderMultiplier = 35;
		this.item = new JLabel[menuSize[myStore]];
		this.itemPrice = new JLabel[menuSize[myStore]];
		
		for(int j = 0; j < menuSize[myStore]; j++)
		{
			numberOfItems[j] = 0;
			
			item[j] = new JLabel(store.getMenuName(myStore, j));
			storeProfilePanel.add(item[j]);
			item[j].setForeground(Color.YELLOW);
			springLayout.putConstraint(SpringLayout.WEST, item[j], 20, SpringLayout.EAST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, item[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);

			itemPrice[j] = new JLabel(store.getMenuPrice(myStore, j) + " NTD");
			storeProfilePanel.add(itemPrice[j]);
			itemPrice[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, itemPrice[j], 10, SpringLayout.EAST, item[j]);
			springLayout.putConstraint(SpringLayout.NORTH, itemPrice[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
		}
		
		//business time
		openDay = new JLabel[7];
		day = new String[7];
		day[0] = "星期一";
		day[1] = "星期二";
		day[2] = "星期三";
		day[3] = "星期四";
		day[4] = "星期五";
		day[5] = "星期六";
		day[6] = "星期日";
		
		for(int k = 0; k < 7; k++)
		{
			openDay[k] = new JLabel(day[k] + " : " + store.getBusinessTime(myStore, k, 0) + " ~ " + store.getBusinessTime(myStore, k, 1));
			storeProfilePanel.add(openDay[k]);
			openDay[k].setForeground(new Color(235, 235, 235));
			openDay[k].setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
			springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, openDay[k], 0, SpringLayout.HORIZONTAL_CENTER, storeProfilePanel);
			springLayout.putConstraint(SpringLayout.NORTH, openDay[k], 3*BorderMultiplier*k/4 + BorderGap + 90, SpringLayout.NORTH, FB);
		}
		
		//show discount
		if(store.getOrderDescription(myStore) != "")
		{
			orderDescription = new JLabel("店家優惠 : " + store.getOrderDescription(myStore));
			storeProfilePanel.add(orderDescription);
			orderDescription.setForeground(new Color(182, 235, 127));
			orderDescription.setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
			springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, orderDescription, 0, SpringLayout.HORIZONTAL_CENTER, storeProfilePanel);
			springLayout.putConstraint(SpringLayout.NORTH, orderDescription, 200 + BorderGap + 90, SpringLayout.NORTH, FB);
		}
	}

	public void initialization()
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class StoreProfilePageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			storeProfilePage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				StoreHomePage HomePage = new StoreHomePage(xStore);
				HomePage.frame.setVisible(true);
			}
			else if(actioncommand.equals("History"))
			{
				storeHistoryPage HistoryPage = new storeHistoryPage();
				HistoryPage.frame.setVisible(true);
			}
		}
	}
	
}
