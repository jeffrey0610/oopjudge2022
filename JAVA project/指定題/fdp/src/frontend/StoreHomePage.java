package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.json.simple.parser.ParseException;

import com.teamdev.jxbrowser.browser.Browser;
import com.teamdev.jxbrowser.engine.Engine;
import com.teamdev.jxbrowser.engine.EngineOptions;
import com.teamdev.jxbrowser.view.swing.BrowserView;

import backend.storeInfoAll;

import static com.teamdev.jxbrowser.engine.RenderingMode.HARDWARE_ACCELERATED;
public class StoreHomePage extends Josephood {

	//see who login
	public static int xStore;
	
	public StoreHomePage() {
		try {
			initialization();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public StoreHomePage(int xstore) {
		xStore = xstore;
		try {
			initialization();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void initialization() throws FileNotFoundException, IOException, ParseException {
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
		StoreHomePageListener StoreHomePageNextListener = new StoreHomePageListener();

		JPanel storeHomePanel = new JPanel();
		storeHomePanel.setBackground(new Color(70, 70, 70));
		frame.add(storeHomePanel);
		
		JButton storeProfileButton = new JButton("Profile");
		storeProfileButton.addActionListener(StoreHomePageNextListener);
		storeHomePanel.add(storeProfileButton);
		
		JButton storeOrdersButton = new JButton("Orders");
		storeOrdersButton.addActionListener(StoreHomePageNextListener);
		storeHomePanel.add(storeOrdersButton);
		
		JButton storeDiscountButton = new JButton("Discounts");
		storeDiscountButton.addActionListener(StoreHomePageNextListener);
		storeHomePanel.add(storeDiscountButton);

		JButton backButton = new JButton("Back");
		backButton.addActionListener(StoreHomePageNextListener);
		storeHomePanel.add(backButton);
		
		// add map
				try {

					String html = storeInfoAll.googleAPI(xStore);
					String htmlPath = System.getProperty("user.dir") + "/map.html";
					String key = "1BNDHFSC1G2ZJ94UPRIP60VYZIPVWNCDM6J41CZ4G99LBHXSS7EKNMI80KMV384LZRFZ5R";

					FileWriter myWriter = new FileWriter(htmlPath);
					myWriter.write(html);
					myWriter.close();
					System.out.println("Successfully wrote to the file.");

					Engine engine = Engine.newInstance(EngineOptions.newBuilder(HARDWARE_ACCELERATED).licenseKey(key).build());
					Browser browser = engine.newBrowser();

					SwingUtilities.invokeLater(() -> {
						final BrowserView view = BrowserView.newInstance(browser);

						JTextField address = new JTextField(htmlPath);
						address.addActionListener(e -> browser.navigation().loadUrl(address.getText()));

						storeHomePanel.add(view);
						frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
						frame.setVisible(true);

						frame.addWindowListener(new WindowAdapter() {
							@Override
							public void windowClosing(WindowEvent e) {
								engine.close();
							}
						});

						browser.navigation().loadUrl(address.getText());
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
	}

	public class StoreHomePageListener implements ActionListener {

		public storeInfoAll store;

		@Override
		public void actionPerformed(ActionEvent e) {

			store = null;
			StoreHomePage.frame.dispose();
			try {
				store = new storeInfoAll();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String actioncommand = e.getActionCommand();

			if (actioncommand.equals("Profile")) {
				storeProfilePage SPP = new storeProfilePage(xStore);
				SPP.frame.setVisible(true);
			} else if (actioncommand.equals("Orders")) {
				try {		
					//pass the order of this store if there is any
					if (me.checkOrder(conn) != null) {						
						for (int w = 0; w < me.getHistoryOrder(conn).size(); w++) {	
							if (me.getHistoryOrder(conn).get(w).storename.equals(store.getName(xStore))) {								
								storeOrdersPage SOP = new storeOrdersPage(me.getHistoryOrder(conn).get(w));
								SOP.frame.setVisible(true);
								break;
							}
						}
					} else {
						storeOrdersPage SOP = new storeOrdersPage(xStore);
						SOP.frame.setVisible(true);
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else if (actioncommand.equals("Discounts")) {
				storeDiscountPage SDP = new storeDiscountPage(xStore);
				SDP.frame.setVisible(true);
			} else if (actioncommand.equals("Back")) {
				Josephood HomePage = new Josephood();
				HomePage.frame.setVisible(true);
			}

		}

	}
}