package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import backend.Order;
import backend.storeInfoAll;
import backend.Order.Status;
import frontend.storeOrdersPage.StoreOrdersPageListener;

public class courierOrdersPage extends CourierHomePage {

	public static Order[] NewOrder;
	public static int OrderNumber = 0;
	public storeInfoAll store;
	public JLabel[] selectItem;
	public JLabel[] selectPrice;
	public JLabel[] selectNumber;
	public JLabel[] selectSum;
	public int[] individulSum;
	public String[] SumString;
	public CourierOrdersPageListener CourierOrdersPageNextListener;

	public static Order thisorder;
	
	public courierOrdersPage()
	{
		initialization();
		Display();
	}

	public courierOrdersPage(Order order)
	{
		if(!order.courierReject && order.storeConfirm)
		{
			thisorder = order;
			initialization();
			Display(order);
		}
		else
		{
			initialization();
			Display();
		}
	}
	
	private void Display() {
		// TODO Auto-generated method stub
		CourierOrdersPageNextListener = new CourierOrdersPageListener();
		JPanel courierHomePanel = new JPanel();
		courierHomePanel.setBackground(new Color(70, 70 , 70));
		courierHomePanel.setLayout(springLayout);
		frame.add(courierHomePanel);
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(CourierOrdersPageNextListener);
		courierHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
	}
	
	private void Display(Order order) {
		// TODO Auto-generated method stub
		JPanel courierHomePanel = new JPanel();
		CourierOrdersPageNextListener = new CourierOrdersPageListener();
		courierHomePanel.setBackground(new Color(70, 70 , 70));
		courierHomePanel.setLayout(springLayout);	
		frame.add(courierHomePanel);
	
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(CourierOrdersPageNextListener);
		courierHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
		//set and vanish confirm/reject buttons
		if(me.checkOrder(conn) != null && !order.courierConfirm && !order.courierReject)
		{
			JButton confirmButton = new JButton("Confirm");
			confirmButton.addActionListener(CourierOrdersPageNextListener);
			courierHomePanel.add(confirmButton);
			springLayout.putConstraint(SpringLayout.WEST, confirmButton, 863, SpringLayout.WEST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, confirmButton, 400, SpringLayout.NORTH, FB);
			
			JButton rejectButton = new JButton("Reject");
			rejectButton.addActionListener(CourierOrdersPageNextListener);
			courierHomePanel.add(rejectButton);
			springLayout.putConstraint(SpringLayout.WEST, rejectButton, 869, SpringLayout.WEST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, rejectButton, 450, SpringLayout.NORTH, FB);
		}
		else if(me.checkOrder(conn) != null && order.courierConfirm)
		{
			JButton tradeDoneButton = new JButton("Trade Done");
			tradeDoneButton.addActionListener(CourierOrdersPageNextListener);
			courierHomePanel.add(tradeDoneButton);
			springLayout.putConstraint(SpringLayout.WEST, tradeDoneButton, 845, SpringLayout.WEST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, tradeDoneButton, 450, SpringLayout.NORTH, FB);
		}
		
		int BorderGap = 70;
		int BorderMultiplier = 35;
		
		selectItem = new JLabel[order.mealsize];
		selectPrice = new JLabel[order.mealsize];
		selectNumber = new JLabel[order.mealsize];
		selectSum = new JLabel[order.mealsize];
		individulSum = new int[order.mealsize];
		SumString = new String[order.mealsize];
		
		//show current order
		for(int j = 0; j < order.mealsize; j++)
		{
			selectItem[j] = new JLabel(order.selectedItems[j]);
			courierHomePanel.add(selectItem[j]);
			selectItem[j].setForeground(Color.YELLOW);
			springLayout.putConstraint(SpringLayout.WEST, selectItem[j], 10, SpringLayout.EAST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, selectItem[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);

			selectPrice[j] = new JLabel(order.selectedPrice[j] + " X ");
			courierHomePanel.add(selectPrice[j]);
			selectPrice[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectPrice[j], 10, SpringLayout.EAST, selectItem[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectPrice[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
			
			String number = String.valueOf(order.selectedNumber[j] + " = ");
			selectNumber[j] = new JLabel(number);
			courierHomePanel.add(selectNumber[j]);
			selectNumber[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectNumber[j], 10, SpringLayout.EAST, selectPrice[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectNumber[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
			
			int price = Integer.parseInt(order.selectedPrice[j]);
			individulSum[j] = price * order.selectedNumber[j];
			SumString[j] = String.valueOf(individulSum[j]);
			selectSum[j] = new JLabel(SumString[j]);
			courierHomePanel.add(selectSum[j]);
			selectSum[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectSum[j], 10, SpringLayout.EAST, selectNumber[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectSum[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
		}
	}

	public void initialization()
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class CourierOrdersPageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			courierOrdersPage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				CourierHomePage HomePage = new CourierHomePage();
				HomePage.frame.setVisible(true);
			}
			else if(actioncommand.equals("Confirm"))
			{
				thisorder.courierConfirm = true;
				courierOrdersPage confirmedPage = new courierOrdersPage(thisorder);
				confirmedPage.frame.setVisible(true);
				try {
					thisorder.changeOrderStatus(thisorder.orderID, conn, Status.Order_is_on_the_way.toString());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			else if(actioncommand.equals("Reject"))
			{
				thisorder.storeReject = true;
				thisorder.courierReject = true;
				courierOrdersPage confirmedPage = new courierOrdersPage(thisorder);
				confirmedPage.frame.setVisible(true);
				try {
					thisorder.changeOrderStatus(thisorder.orderID, conn, Status.Order_failed.toString());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			else if(actioncommand.equals("Trade Done"))
			{
				thisorder.storeReject = true;
				thisorder.courierReject = true;
				courierOrdersPage confirmedPage = new courierOrdersPage(thisorder);
				confirmedPage.frame.setVisible(true);
				try {
					thisorder.changeOrderStatus(thisorder.orderID, conn, Status.Order_is_arrived.toString());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}

	}
}
