package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

import backend.Consumer;
import backend.Order;
import backend.memberInfoAll;
import frontend.memberHistoryPage.MemberHistoryPageListener;

public class courierHistoryPage extends courierProfilePage{
	//History is in profile page
	public static double lat = 25.017;
	public static double lon = 121.543;
	public memberInfoAll member;
	
	public courierHistoryPage()
	{
		initialization();
	}

	public void initialization()
	{
		member = null;
		try {
			member = new memberInfoAll();
		} catch (IOException | ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
		CourierHistoryPageListener CourierHistoryPageNextListener = new CourierHistoryPageListener();
		
		JPanel memberHistoryPanel = new JPanel();
		memberHistoryPanel.setBackground(new Color(70, 70 , 70));
		memberHistoryPanel.setLayout(springLayout);
		frame.add(memberHistoryPanel);
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(CourierHistoryPageNextListener);
		memberHistoryPanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
		Consumer consumer = new Consumer();
		int loop = 0;
		ArrayList<Order> orderMenu = null;
		try {
			loop = consumer.getHistoryOrder(conn).size();
			orderMenu = consumer.getHistoryOrder(conn);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int space = 20;
		
		//i orders
		for(int i = 0; i < loop; i++)
		{
			JLabel[] orderName = new JLabel[orderMenu.get(i).mealsize];
			JLabel[] orderPrice = new JLabel[orderMenu.get(i).mealsize];
			JLabel[] orderNumber = new JLabel[orderMenu.get(i).mealsize];
			for(int j = 0; j < orderMenu.get(i).mealsize; j++)
			{
				try {
					orderName[j] = new JLabel(consumer.getHistoryOrder(conn).get(i).selectedItems[j] + " ");
					orderPrice[j] = new JLabel(consumer.getHistoryOrder(conn).get(i).selectedPrice[j] + " X ");
					orderNumber[j] = new JLabel(String.valueOf(consumer.getHistoryOrder(conn).get(i).selectedNumber[j]));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				memberHistoryPanel.add(orderName[j]);
				orderName[j].setForeground(new Color(211, 220, 47));
				springLayout.putConstraint(SpringLayout.WEST, orderName[j], 20 + 170*i, SpringLayout.WEST, FB);
				springLayout.putConstraint(SpringLayout.NORTH, orderName[j], space*j + 40, SpringLayout.NORTH, FB);
				
				memberHistoryPanel.add(orderPrice[j]);
				orderPrice[j].setForeground(new Color(211, 220, 47));
				springLayout.putConstraint(SpringLayout.WEST, orderPrice[j], 10, SpringLayout.EAST, orderName[j]);
				springLayout.putConstraint(SpringLayout.NORTH, orderPrice[j], space*j + 40, SpringLayout.NORTH, FB);
				
				memberHistoryPanel.add(orderNumber[j]);
				orderNumber[j].setForeground(new Color(211, 220, 47));
				springLayout.putConstraint(SpringLayout.WEST, orderNumber[j], 3, SpringLayout.EAST, orderPrice[j]);
				springLayout.putConstraint(SpringLayout.NORTH, orderNumber[j], space*j + 40, SpringLayout.NORTH, FB);
				
				orderName[j] = null;
				orderPrice[j] = null;
				orderNumber[j] = null;
			}
		}
	}
	
	public class CourierHistoryPageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			memberHistoryPage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				courierProfilePage courierPage = new courierProfilePage();
				courierPage.frame.setVisible(true);
			}	
		}
	}
}
