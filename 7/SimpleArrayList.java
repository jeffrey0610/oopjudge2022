/**
 * This class uses an Integer array as its internal instance variable to save integer, 
 * and its size needs to be changed automatically based on user's manipulation.
 * @author jeffr
 *
 */
public class SimpleArrayList {
	/**
	 * @param x
	 * Integer型別的x陣列
	 * @param size
	 * x陣列的大小
	 */
	public int size;
	public Integer[] x;
	
	/**
	 * 若沒附值，size的起始值為0
	 */
	public SimpleArrayList() {
		this.size=0;
	}
	
	/**
	 * 創造一個size大小的x陣列
	 * 且其成員為皆為0
	 */
	public SimpleArrayList(int size) {
		this.size=size;
		x=new Integer[size];
		for(int i=0 ; i<size ; i++) {
			x[i]=0;
		}
	}
	
	/**
	 * 在x陣列加入一個元素i
	 * 且size加一
	 * @param i 要多加的元素
	 */
	public void add(Integer i){
		size++;
		Integer[] y=new Integer[size];
		for(int j=0 ; j<size-1 ; j++) y[j]=x[j];
		y[size-1]=i;
		x=y;
	}
	
	/**
	 * 讀取第index項的值
	 * @param index 要讀取的位置
	 * @return 若index不再範圍內，回傳null。則回傳第index項的值
	 */
	public Integer get(int index){
		if(index>=0 && index<size) return x[index];
		else return null;
	}
	
	/**
	 * 將第index項的值取代成element
	 * @param index  要讀取的位置
	 * @param element 要取代成的值
	 * @return 若index不再範圍內，回傳null。則回傳原來被取代的值
	 */
	public Integer set(int index, Integer element){
		if(index<0 || index>=size) return null;
		Integer i = x[index];
		x[index]=element;
		return i;
	}
	
	/**
	 * 移除第index項的值，且size減一
	 * @param index 要移除的位置
	 * @return 若原本的值為null，則回傳false，其餘則回傳true
	 */
	public boolean remove(int index){
		if( x[index] == null ) return false;
		else {
			Integer[] y=new Integer[size--];
			for(int i=0 ; i<index ; i++)       y[i]=x[i];
			for(int i=index ; i<size ; i++)  y[i]=x[i+1];
			x=y;
			return true;
		}
	}
	
	/**
	 * 將x清除，且size變成0
	 */
	public void clear(){
		size=0;
		SimpleArrayList y = new SimpleArrayList(size);
		this.x=y.x;
	}
	
	/**
	 * 回傳size值
	 * @return 回傳size值
	 */
	public int size(){
		 return size;
	 }
	
	/**
	 * 一一比對，先假設每一項都是false，若x裡的成員也是l的x的成員，則變true，最後將所有false的成員移除。
	 * @param l 要比對的另一個陣列
	 * @return 若不變則回傳false，其餘則true
	 */
	public boolean retainAll(SimpleArrayList l) {
		 boolean[]a=new boolean[this.size];		 
		 for(int k=0 ; k<this.size ; k++) a[k]=false;
		 for(int i=0 ; i<l.size ; i++) {
			 for(int j=0 ; j<this.size ; j++) {
				 if(l.x[i]==this.x[j]) a[j]=true;
			 }
		 }
		 SimpleArrayList y = new SimpleArrayList();
		 for(int i=0 ; i<a.length ; i++) {
			 if(a[i]==true) y.add(this.x[i]);
		 }
		 size=y.x.length;
		 this.x=y.x;
		 if(a.length == this.x.length)return false;
		 else return true;
	 }
}
