package frontend;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

public class CourierLoginPage extends Josephood{
	
	public JTextField CourierNameBox;
	public JTextField CourierPasswordBox;
	public courierLoginPageListener LoginPageNextListener;
	public JPanel LoginPanel;
	public int BoxWidth = 25;
	public courierInfoAll courier;
	private String password;
	private String DirectedStore;
	private int tracker = 0;
	
	public CourierLoginPage()
	{
		initialization();
		loginSet();
	}
	
	public CourierLoginPage(int a)
	{
		initialization();
		loginSet();
		TryAgain();
	}
	
	
	
	private void TryAgain() {

		JLabel error = new JLabel("Invalid Account Name Or Password Entered, Please Try Agian.");
		LoginPanel.add(error);
		error.setForeground(Color.GREEN);
		springLayout.putConstraint(SpringLayout.WEST, error, 311, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, error, 32, SpringLayout.NORTH, CourierPasswordBox);
		
	}

	private void loginSet() {
		
		LoginPageNextListener = new courierLoginPageListener();
		
		LoginPanel = new JPanel();
		LoginPanel.setBackground(new Color(70, 70 , 70));
		frame.add(LoginPanel);
		LoginPanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		this.CourierNameBox = new JTextField("Please Enter The Full Name Of Your Store", BoxWidth);
		LoginPanel.add(CourierNameBox);
		CourierNameBox.setBackground(Color.WHITE);
		CourierNameBox.addActionListener(LoginPageNextListener);
		springLayout.putConstraint(SpringLayout.WEST, CourierNameBox, 350, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, CourierNameBox, 260, SpringLayout.NORTH, FB);
		
		this.CourierPasswordBox = new JTextField("Please Enter Your Store Password", BoxWidth);
		LoginPanel.add(CourierPasswordBox);
		CourierPasswordBox.setBackground(Color.WHITE);
		CourierPasswordBox.addActionListener(LoginPageNextListener);
		springLayout.putConstraint(SpringLayout.WEST, CourierPasswordBox, 350, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, CourierPasswordBox, 340, SpringLayout.NORTH, FB);
		
		JLabel accountTitle = new JLabel("Account");
		LoginPanel.add(accountTitle);
		accountTitle.setForeground(Color.WHITE);
		accountTitle.setFont(new Font("Serif", Font.PLAIN, 18));
		springLayout.putConstraint(SpringLayout.WEST, accountTitle, 350, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, accountTitle, 235, SpringLayout.NORTH, FB);
		
		JLabel passwordTitle = new JLabel("Password");
		LoginPanel.add(passwordTitle);
		passwordTitle.setForeground(Color.WHITE);
		passwordTitle.setFont(new Font("Serif", Font.PLAIN, 18));
		springLayout.putConstraint(SpringLayout.WEST, passwordTitle, 350, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, passwordTitle, 315, SpringLayout.NORTH, FB);
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(LoginPageNextListener);
		LoginPanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
		ImageIcon iconn = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/scooter.png");
		Image transformn = iconn.getImage();
		Image StoreImagen = transformn.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH);
		ImageIcon StoreIconn = new ImageIcon(StoreImagen);
		JLabel storeiconn = new JLabel(StoreIconn);
		LoginPanel.add(storeiconn);
		springLayout.putConstraint(SpringLayout.WEST, storeiconn, 422, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeiconn, 90, SpringLayout.NORTH, FB);
		
		
		ImageIcon icon = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/47767ee081846bbf07479978b111b6d9-modified.png");
		Image transform = icon.getImage();
		Image StoreImage = transform.getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
		ImageIcon StoreIcon = new ImageIcon(StoreImage);
		JLabel storeicon = new JLabel(StoreIcon);
		LoginPanel.add(storeicon);
		springLayout.putConstraint(SpringLayout.WEST, storeicon, 405, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeicon, 80, SpringLayout.NORTH, FB);
		
		ImageIcon gicon = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/light-gray-circle-icon-926957.png");
		Image gtransform = gicon.getImage();
		Image gStoreImage = gtransform.getScaledInstance(158, 158, java.awt.Image.SCALE_SMOOTH);
		ImageIcon gStoreIcon = new ImageIcon(gStoreImage);
		JLabel gstoreicon = new JLabel(gStoreIcon);
		LoginPanel.add(gstoreicon);
		springLayout.putConstraint(SpringLayout.WEST, gstoreicon, 401, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, gstoreicon, 76, SpringLayout.NORTH, FB);
	}

	public void initialization()
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class courierLoginPageListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			MemberHomePage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				Josephood HomePage = new Josephood();
				HomePage.frame.setVisible(true);
			}
			else
			{
				courier = null;
				try {
					courier = new courierInfoAll();
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try
				{
					for(int i = 0; i <= courier.getName().length; i++)
					{	
						if(CourierNameBox.getText().equals(courier.getName(i).toString()))
						{
							DirectedStore = courier.getName(i).toString();
							tracker = i;
							break;
						}
						else if(i > courier.getName().length)
						{
							throw new Exception("WrongName");
						}
					}
					
					for(int j = 0; j <= courier.getName().length; j++)
					{	
						System.out.println(courier.getPassword(tracker));
						if(CourierPasswordBox.getText().equals(courier.getPassword(tracker)))
						{
							CourierHomePage GoToMember = new CourierHomePage(tracker);
							GoToMember.frame.setVisible(true);
							break;
						}
						else
						{
							throw new Exception("WrongName");
						}
					}
				}
				catch(Exception ee)
				{
					CourierLoginPage NEW = new CourierLoginPage(1);
					NEW.frame.setVisible(true);
				}
			}	
		}		
	}
}
