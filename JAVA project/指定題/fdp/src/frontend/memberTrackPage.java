package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import backend.Order;

public class memberTrackPage extends MemberHomePage{

	//Track placed order
	public JLabel[] selectItem;
	public JLabel[] selectPrice;
	public JLabel[] selectNumber;
	public JLabel[] selectSum;
	public int[] individulSum;
	public String[] SumString;
	public StoreOrdersPageListener MemberTrackPageNextListener;
	public JPanel memberTrackPanel;
	
	public memberTrackPage(Order order)
	{
		if(!order.courierReject && !order.storeReject)
		{
			initialization();
			Display(order);
		}
		else
		{
			initialization();
			defaultDisplay();
		}
	}
	
	public memberTrackPage()
	{
		initialization();
		defaultDisplay();
	}
	
	private void defaultDisplay() {
		// TODO Auto-generated method stub
		MemberTrackPageNextListener = new StoreOrdersPageListener();
		memberTrackPanel = new JPanel();
		memberTrackPanel.setBackground(new Color(70, 70 , 70));
		memberTrackPanel.setLayout(springLayout);
		frame.add(memberTrackPanel);
		
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(MemberTrackPageNextListener);
		memberTrackPanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
	}

	private void Display(Order order) {

		MemberTrackPageNextListener = new StoreOrdersPageListener();
		memberTrackPanel = new JPanel();
		memberTrackPanel.setBackground(new Color(70, 70 , 70));
		memberTrackPanel.setLayout(springLayout);	
		frame.add(memberTrackPanel);
		
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(MemberTrackPageNextListener);
		memberTrackPanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
		int BorderGap = 70;
		int BorderMultiplier = 35;
		
		selectItem = new JLabel[order.mealsize];
		selectPrice = new JLabel[order.mealsize];
		selectNumber = new JLabel[order.mealsize];
		selectSum = new JLabel[order.mealsize];
		individulSum = new int[order.mealsize];
		SumString = new String[order.mealsize];
		
		//track the order 
		if(order.storeConfirm && !order.courierConfirm)
		{
			JLabel storeconfirm = new JLabel("The Restaurant Has Confirmed Your Order");
			memberTrackPanel.add(storeconfirm);
			storeconfirm.setForeground(Color.YELLOW);
			springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, storeconfirm, 0, SpringLayout.HORIZONTAL_CENTER, memberTrackPanel);
			springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, storeconfirm, 0, SpringLayout.VERTICAL_CENTER, memberTrackPanel);
		}
		else if(order.courierConfirm)
		{
			JLabel courierconfirm = new JLabel("The Courier Has Confirmed Your Order");
			memberTrackPanel.add(courierconfirm);
			courierconfirm.setForeground(Color.YELLOW);
			springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, courierconfirm, 0, SpringLayout.HORIZONTAL_CENTER, memberTrackPanel);
			springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, courierconfirm, 0, SpringLayout.VERTICAL_CENTER, memberTrackPanel);
		}
		
		//show placed order
		for(int j = 0; j < order.mealsize; j++)
		{
			selectItem[j] = new JLabel(order.selectedItems[j]);
			memberTrackPanel.add(selectItem[j]);
			selectItem[j].setForeground(Color.YELLOW);
			springLayout.putConstraint(SpringLayout.WEST, selectItem[j], 10, SpringLayout.EAST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, selectItem[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);

			selectPrice[j] = new JLabel(order.selectedPrice[j] + " X ");
			memberTrackPanel.add(selectPrice[j]);
			selectPrice[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectPrice[j], 10, SpringLayout.EAST, selectItem[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectPrice[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
			
			String number = String.valueOf(order.selectedNumber[j] + " = ");
			selectNumber[j] = new JLabel(number);
			memberTrackPanel.add(selectNumber[j]);
			selectNumber[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectNumber[j], 10, SpringLayout.EAST, selectPrice[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectNumber[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
			
			int price = Integer.parseInt(order.selectedPrice[j]);
			individulSum[j] = price * order.selectedNumber[j];
			SumString[j] = String.valueOf(individulSum[j]);
			selectSum[j] = new JLabel(SumString[j]);
			memberTrackPanel.add(selectSum[j]);
			selectSum[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectSum[j], 10, SpringLayout.EAST, selectNumber[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectSum[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
		}
		
	}

	public void initialization()
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class StoreOrdersPageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			MemberHomePage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				MemberHomePage HomePage = new MemberHomePage(memberTrack);
				HomePage.frame.setVisible(true);
			}
			
		}

	}
	
}
