
/**
 * Square 繼承 Shape class
 * 
 * @author jeffr
 *
 */
public class Square extends Shape {
	/**
	 * 設定Square的邊長初始值
	 * @param length
	 * Square的邊長
	 */
	public Square(double length) {
		super(length);
	}
	/**
	 * 重新設定Square的邊長
	 */
	public void setLength(double length) {
		this.length=length;
	}
	/**
	 * Square的面積
	 * @return
	 * 回傳Square的面積 = length*length
	 */
	public double getArea() {
		double area = length*length;
		return (Math.round(area*100)/100.0);
	}
	/**
	 * Square的周長
	 * @return
	 * 回傳Square的周長 = 4*length
	 */
	public double getPerimeter() {
		return 4*length;
	}
}
