package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

public class MemberHomePage extends Josephood{
	//Simply an interface of member
	//memberTrack is set to identify the member who login.
	public static int memberTrack;
	
	public MemberHomePage()
	{
		try {
			initialization(1);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public MemberHomePage(int a)
	{
		memberTrack = a;
		try {
			initialization(a);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void initialization(int a) throws FileNotFoundException, IOException, ParseException
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
		MemberHomePageListener MemberHomePageNextListener = new MemberHomePageListener();
		
		JPanel memberHomePanel = new JPanel();
		memberHomePanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberHomePanel);
		memberHomePanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		int dis = 278;
		JButton memberProfileButton = new JButton("Profile");
		memberProfileButton.addActionListener(MemberHomePageNextListener);
		memberHomePanel.add(memberProfileButton);
		springLayout.putConstraint(SpringLayout.WEST, memberProfileButton, dis, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, memberProfileButton, 410, SpringLayout.NORTH, FB);
		
		JButton memberBrowseButton = new JButton("Browse");
		memberBrowseButton.addActionListener(MemberHomePageNextListener);
		memberHomePanel.add(memberBrowseButton);
		springLayout.putConstraint(SpringLayout.WEST, memberBrowseButton, dis + 118, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, memberBrowseButton, 410, SpringLayout.NORTH, FB);
		
		JButton memberTrackButton = new JButton("Track");
		memberTrackButton.addActionListener(MemberHomePageNextListener);
		memberHomePanel.add(memberTrackButton);
		springLayout.putConstraint(SpringLayout.WEST, memberTrackButton, dis + 239, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, memberTrackButton, 410, SpringLayout.NORTH, FB);
		
		JButton memberSearchButton = new JButton("Search");
		memberSearchButton.addActionListener(MemberHomePageNextListener);
		memberHomePanel.add(memberSearchButton);
		springLayout.putConstraint(SpringLayout.WEST, memberSearchButton, dis + 350, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, memberSearchButton, 410, SpringLayout.NORTH, FB);
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(MemberHomePageNextListener);
		memberHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
		ImageIcon iconi = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/drink@2x.png");
		Image transformi = iconi.getImage();
		Image StoreImagei = transformi.getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
		ImageIcon StoreIconi = new ImageIcon(StoreImagei);
		JLabel storeiconi = new JLabel(StoreIconi);
		memberHomePanel.add(storeiconi);
		springLayout.putConstraint(SpringLayout.WEST, storeiconi, 400, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeiconi, 140, SpringLayout.NORTH, FB);
		
		ImageIcon icon = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/2022-06-13 170350-modified.png");
		Image transform = icon.getImage();
		Image StoreImage = transform.getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
		ImageIcon StoreIcon = new ImageIcon(StoreImage);
		JLabel storeicon = new JLabel(StoreIcon);
		memberHomePanel.add(storeicon);
		springLayout.putConstraint(SpringLayout.WEST, storeicon, 402, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeicon, 140, SpringLayout.NORTH, FB);
		
		ImageIcon gicon = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/light-gray-circle-icon-926957.png");
		Image gtransform = gicon.getImage();
		Image gStoreImage = gtransform.getScaledInstance(158, 158, java.awt.Image.SCALE_SMOOTH);
		ImageIcon gStoreIcon = new ImageIcon(gStoreImage);
		JLabel gstoreicon = new JLabel(gStoreIcon);
		memberHomePanel.add(gstoreicon);
		springLayout.putConstraint(SpringLayout.WEST, gstoreicon, 398, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, gstoreicon, 136, SpringLayout.NORTH, FB);
		
		JLabel food = new JLabel("Cook It With care, Well Done Not Rare");
		memberHomePanel.add(food);
		food.setFont(new Font("Serif", Font.CENTER_BASELINE, 25));
		food.setForeground(new Color(206, 207, 130));
		springLayout.putConstraint(SpringLayout.WEST, food, 281, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, food, 320, SpringLayout.NORTH, FB);
	}
	
	public class MemberHomePageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			MemberHomePage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Profile"))
			{
				memberProfilePage MPP = new memberProfilePage();
				MPP.frame.setVisible(true);
			}
			else if(actioncommand.equals("Browse"))
			{
				memberBrowsePage MBP = new memberBrowsePage();
				MBP.frame.setVisible(true);
			}
			else if(actioncommand.equals("Track"))
			{
				//To check if there are any ongoing order in the database
				if(me.checkOrder(conn) != null)
				{
					//me.checkOrder(conn) is the order in Order class that is saved to database as placed order.
					memberTrackPage MTP = new memberTrackPage(me.checkOrder(conn));
					MTP.frame.setVisible(true);
				}
				else
				{
					memberTrackPage MTP = new memberTrackPage();
					MTP.frame.setVisible(true);
				}
			}
			else if(actioncommand.equals("Search"))
			{
				memberSearchPage MSP = new memberSearchPage();
				MSP.frame.setVisible(true);
			}
			else if(actioncommand.equals("Back"))
			{
				Josephood HomePage = new Josephood();
				HomePage.frame.setVisible(true);
			}
			
		}

	}

}
