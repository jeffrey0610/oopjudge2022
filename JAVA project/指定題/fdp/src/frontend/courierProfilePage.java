package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

public class courierProfilePage extends CourierHomePage {
	
	public courierInfoAll courier;
	
	public courierProfilePage()
	{
		initialization();
	}

	public void initialization()
	{
		courier = null;
		try {
			courier = new courierInfoAll();
		} catch (IOException | ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
		CourierProfilePageListener CourierProfilePageNextListener = new CourierProfilePageListener();
		
		JPanel courierHomePanel = new JPanel();
		courierHomePanel.setBackground(new Color(70, 70 , 70));
		courierHomePanel.setLayout(springLayout);	
		frame.add(courierHomePanel);
		FB = frame.getContentPane();
		
		//buttons
		JButton backButton = new JButton("Back");
		backButton.addActionListener(CourierProfilePageNextListener);
		courierHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
	
		JButton historyButton = new JButton("History");
		historyButton.addActionListener(CourierProfilePageNextListener);
		courierHomePanel.add(historyButton);
		springLayout.putConstraint(SpringLayout.WEST, historyButton, 867, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, historyButton, 450, SpringLayout.NORTH, FB);
		
		//profile info
		int space = 165;
		JLabel CourierName = new JLabel(courier.getName(courierTrack));
		courierHomePanel.add(CourierName);
		CourierName.setFont(new Font("Serif", Font.BOLD, 60));
		CourierName.setForeground(new Color(211, 220, 47));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, CourierName, 0, SpringLayout.HORIZONTAL_CENTER, courierHomePanel);
		springLayout.putConstraint(SpringLayout.NORTH, CourierName, space + 40, SpringLayout.NORTH, FB);
		
		JLabel account = new JLabel("帳號：ESOEoopFINALproject");
		courierHomePanel.add(account);
		account.setForeground(new Color(235, 235, 235));
		account.setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, account, 0, SpringLayout.HORIZONTAL_CENTER, courierHomePanel);
		springLayout.putConstraint(SpringLayout.NORTH, account, space + 125, SpringLayout.NORTH, FB);
		
		JLabel password = new JLabel("密碼：" + courier.getPassword(courierTrack));
		courierHomePanel.add(password);
		password.setForeground(new Color(235, 235, 235));
		password.setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, password, 0, SpringLayout.HORIZONTAL_CENTER, courierHomePanel);
		springLayout.putConstraint(SpringLayout.NORTH, password, space + 155, SpringLayout.NORTH, FB);
		
		JLabel phone = new JLabel("電話：" + courier.getPhone(courierTrack));
		courierHomePanel.add(phone);
		phone.setForeground(new Color(235, 235, 235));
		phone.setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, phone, 0, SpringLayout.HORIZONTAL_CENTER, courierHomePanel);
		springLayout.putConstraint(SpringLayout.NORTH, phone, space + 185, SpringLayout.NORTH, FB);

		JLabel email = new JLabel("信箱：" + courier.getEmail(courierTrack));
		courierHomePanel.add(email);
		email.setForeground(new Color(235, 235, 235));
		email.setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, email, 0, SpringLayout.HORIZONTAL_CENTER, courierHomePanel);
		springLayout.putConstraint(SpringLayout.NORTH, email, space + 215, SpringLayout.NORTH, FB);
		
		JLabel slogan = new JLabel(courier.getName(courierTrack) + "為您服務");
		courierHomePanel.add(slogan);
		slogan.setForeground(new Color(112, 217, 89));
		slogan.setFont(new Font("Serif", Font.HANGING_BASELINE, 16));
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, slogan, 0, SpringLayout.HORIZONTAL_CENTER, courierHomePanel);
		springLayout.putConstraint(SpringLayout.NORTH, slogan, space + 265, SpringLayout.NORTH, FB);
		
		ImageIcon icon = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/158605-modified.png");
		Image transform = icon.getImage();
		Image StoreImage = transform.getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
		ImageIcon StoreIcon = new ImageIcon(StoreImage);
		JLabel storeicon = new JLabel(StoreIcon);
		courierHomePanel.add(storeicon);
		springLayout.putConstraint(SpringLayout.WEST, storeicon, 420, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeicon, 50, SpringLayout.NORTH, FB);
		
		ImageIcon gicon = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/light-gray-circle-icon-926957.png");
		Image gtransform = gicon.getImage();
		Image gStoreImage = gtransform.getScaledInstance(158, 158, java.awt.Image.SCALE_SMOOTH);
		ImageIcon gStoreIcon = new ImageIcon(gStoreImage);
		JLabel gstoreicon = new JLabel(gStoreIcon);
		courierHomePanel.add(gstoreicon);
		springLayout.putConstraint(SpringLayout.WEST, gstoreicon, 416, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, gstoreicon, 46, SpringLayout.NORTH, FB);
	}
	
	public class CourierProfilePageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			courierProfilePage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				CourierHomePage HomePage = new CourierHomePage();
				HomePage.frame.setVisible(true);
			}
			else if(actioncommand.equals("History"))
			{
				courierHistoryPage HistoryPage = new courierHistoryPage();
				HistoryPage.frame.setVisible(true);
			}
		}

	}
}
