package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

import backend.categoryInfo;
import backend.storeInfoAll;

public class memberSearchPage extends MemberHomePage{

	public JTextField SearchBox;
	public int BoxWidth = 25;
	public JPanel memberSearchPanel;
	public JLabel error;
	public JMenu TypeMenu;
	public JMenuBar TypeMenuBar;
	public JMenuItem StoreNameItem;
	public JMenuItem CategoryItem;
	public JMenuItem DeliveryTimeItem;
	public JMenuItem DiscountItem;
	public MemberSearchPageListener MemberSearchPageNextListener;
	public storeInfoAll store;
	public JButton[] CategoryButton;
	public categoryInfo categoryData;
	private boolean which;
	public JButton backButton;
	public TimeSearchListener TimeSearchNextListener;
	
	public static JButton Ten;
	public static JButton Twenty;
	public static JButton Thirty;
	public static JButton Fourty;
	public static JButton Fifty;
	public static JButton Sixty;
	
	public memberSearchPage()
	{
		initialization();
		searchBox();
		MenuSet();
		which = false;
	}

	public memberSearchPage(int a)
	{
		initialization();
		searchBox();
		MenuSet();
		New();
		which = false;
	}
	
	public memberSearchPage(int a, int b)
	{
		initialization();
		CategorySet();
		MenuSet();
		which = true;
	}
	
	public memberSearchPage(int a, int b, int c)
	{
		initialization();
		TimeSet();
		MenuSet();
		which = false;
	}

	public memberSearchPage(int a, int b, int c, int d)
	{
		initialization();
		DiscountSet();
		MenuSet();
		which = false;
	}
	
	public memberSearchPage(int a, int b, int c, int d, int e)
	{
		initialization();
		NoDiscount();
		MenuSet();
		which = false;
	}
	
	private void NoDiscount() {
		// TODO Auto-generated method stub
		MemberSearchPageNextListener = new MemberSearchPageListener();
		TimeSearchNextListener = new TimeSearchListener();
		
		memberSearchPanel = new JPanel();
		memberSearchPanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberSearchPanel);
		memberSearchPanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		JLabel No = new JLabel("There Are Currently No Discount Event");
		memberSearchPanel.add(No);
		No.setForeground(new Color(232, 171, 128));
		No.setFont(new Font("Serif", Font.PLAIN, 20));
		springLayout.putConstraint(SpringLayout.WEST, No, 325, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, No, 220, SpringLayout.NORTH, FB);
		
		backButton = new JButton("Back");
		backButton.addActionListener(MemberSearchPageNextListener);
		memberSearchPanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
	}

	private void TimeSet() {
		
		int mul = 40;
		int ini = 130;
		MemberSearchPageNextListener = new MemberSearchPageListener();
		TimeSearchNextListener = new TimeSearchListener();
		
		memberSearchPanel = new JPanel();
		memberSearchPanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberSearchPanel);
		memberSearchPanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		Ten = new JButton("10 minutes");
		Ten.addActionListener(TimeSearchNextListener);
		memberSearchPanel.add(Ten);
		springLayout.putConstraint(SpringLayout.WEST, Ten, 450, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, Ten, ini, SpringLayout.NORTH, FB);
		
		Twenty = new JButton("20 minutes");
		Twenty.addActionListener(TimeSearchNextListener);
		memberSearchPanel.add(Twenty);
		springLayout.putConstraint(SpringLayout.WEST, Twenty, 450, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, Twenty, ini + mul, SpringLayout.NORTH, FB);
		
		Thirty = new JButton("30 minutes");
		Thirty.addActionListener(TimeSearchNextListener);
		memberSearchPanel.add(Thirty);
		springLayout.putConstraint(SpringLayout.WEST, Thirty, 450, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, Thirty, ini + 2*mul, SpringLayout.SOUTH, FB);
		
		Fourty = new JButton("40 minutes");
		Fourty.addActionListener(TimeSearchNextListener);
		memberSearchPanel.add(Fourty);
		springLayout.putConstraint(SpringLayout.WEST, Fourty, 450, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, Fourty, ini + 3*mul, SpringLayout.SOUTH, FB);
		
		Fifty = new JButton("50 minutes");
		Fifty.addActionListener(TimeSearchNextListener);
		memberSearchPanel.add(Fifty);
		springLayout.putConstraint(SpringLayout.WEST, Fifty, 450, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, Fifty, ini + 4*mul, SpringLayout.SOUTH, FB);
		
		Sixty = new JButton("60 minutes");
		Sixty.addActionListener(TimeSearchNextListener);
		memberSearchPanel.add(Sixty);
		springLayout.putConstraint(SpringLayout.WEST, Sixty, 450, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, Sixty, ini + 5*mul, SpringLayout.SOUTH, FB);
		
		backButton = new JButton("Back");
		backButton.addActionListener(MemberSearchPageNextListener);
		memberSearchPanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
	}

	private void DiscountSet() {

		MemberSearchPageNextListener = new MemberSearchPageListener();
		
		memberSearchPanel = new JPanel();
		memberSearchPanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberSearchPanel);
		memberSearchPanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		JButton BOGOF = new JButton("Buy 1 Get 1 Free");
		BOGOF.addActionListener(MemberSearchPageNextListener);
		memberSearchPanel.add(BOGOF);
		springLayout.putConstraint(SpringLayout.WEST, BOGOF, 345, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, BOGOF, 220, SpringLayout.NORTH, FB);
		
		JButton OFF = new JButton("20 % off For All Items");
		OFF.addActionListener(MemberSearchPageNextListener);
		memberSearchPanel.add(OFF);
		springLayout.putConstraint(SpringLayout.WEST, OFF, 20, SpringLayout.EAST, BOGOF);
		springLayout.putConstraint(SpringLayout.NORTH, OFF, 220, SpringLayout.NORTH, FB);
		
		backButton = new JButton("Back");
		backButton.addActionListener(MemberSearchPageNextListener);
		memberSearchPanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
	}

	private void CategorySet() {
		
		MemberSearchPageNextListener = new MemberSearchPageListener();
		memberSearchPanel = new JPanel();
		memberSearchPanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberSearchPanel);
		
		categoryData = null;
		try {
			categoryData = new categoryInfo();
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		CategoryButton = new JButton[categoryData.getCategory().length];
		
		for(int i = 0; i < categoryData.getCategory().length ; i++)
		{
			CategoryButton[i] = new JButton(categoryData.getCategory(i).toString());
			CategoryButton[i].addActionListener(MemberSearchPageNextListener);
			memberSearchPanel.add(CategoryButton[i]);
			CategoryButton[i].setBackground(new Color(230, 240, 125));
		}
		
		backButton = new JButton("Back");
		backButton.addActionListener(MemberSearchPageNextListener);
		memberSearchPanel.add(backButton);
		
	}

	private void MenuSet() {
		
		TypeMenu = new JMenu("Search By");
		TypeMenuBar = new JMenuBar();
		TypeMenuBar.add(TypeMenu);
		frame.setJMenuBar(TypeMenuBar);
		
		StoreNameItem = new JMenuItem("Store Name");
		StoreNameItem.addActionListener(MemberSearchPageNextListener);
		TypeMenu.add(StoreNameItem);
		
		CategoryItem = new JMenuItem("Cuisine Type");
		CategoryItem.addActionListener(MemberSearchPageNextListener);
		TypeMenu.add(CategoryItem);
		
		DeliveryTimeItem = new JMenuItem("Delivery Time");
		DeliveryTimeItem.addActionListener(MemberSearchPageNextListener);
		TypeMenu.add(DeliveryTimeItem);
		
		DiscountItem = new JMenuItem("Discount");
		DiscountItem.addActionListener(MemberSearchPageNextListener);
		TypeMenu.add(DiscountItem);
		
	}
	
	private void New() {
		
		error = new JLabel("Incorrect Name Entered, Please Try Agian.");
		memberSearchPanel.add(error);
		error.setForeground(Color.GREEN);
		springLayout.putConstraint(SpringLayout.WEST, error, 365, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, error, 30, SpringLayout.NORTH, SearchBox);
		
	}

	public void searchBox() {
		
		MemberSearchPageNextListener = new MemberSearchPageListener();
		
		memberSearchPanel = new JPanel();
		memberSearchPanel.setBackground(new Color(70, 70 , 70));
		frame.add(memberSearchPanel);
		memberSearchPanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		this.SearchBox = new JTextField("Please Enter The Full Name Of The Stores", BoxWidth);
		memberSearchPanel.add(SearchBox);
		SearchBox.setBackground(Color.WHITE);
		springLayout.putConstraint(SpringLayout.WEST, SearchBox, 360, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, SearchBox, 220, SpringLayout.NORTH, FB);
		
		SearchBox.addActionListener(MemberSearchPageNextListener);
		
		backButton = new JButton("Back");
		backButton.addActionListener(MemberSearchPageNextListener);
		memberSearchPanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 490, SpringLayout.NORTH, FB);
	
	}

	public void initialization()
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class TimeSearchListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			MemberHomePage.frame.dispose();
			Object actionsource = e.getSource();
			String actioncommand = e.getActionCommand();
			int time = 0;
			
			if(actionsource == memberSearchPage.Ten)
			{
				time = 10;
				memberBrowsePage BrowsePage = new memberBrowsePage(10, memberHistoryPage.lat, memberHistoryPage.lon);
				BrowsePage.frame.setVisible(true);
			}
			if(actionsource == memberSearchPage.Twenty)
			{
				time = 20;
				memberBrowsePage BrowsePage = new memberBrowsePage(20, memberHistoryPage.lat, memberHistoryPage.lon);
				BrowsePage.frame.setVisible(true);
			}
			else if(actionsource == memberSearchPage.Thirty)
			{
				time = 30;
				memberBrowsePage BrowsePage = new memberBrowsePage(30, memberHistoryPage.lat, memberHistoryPage.lon);
				BrowsePage.frame.setVisible(true);
			}
			else if(actionsource == memberSearchPage.Fourty)
			{
				time = 40;
				memberBrowsePage BrowsePage = new memberBrowsePage(40, memberHistoryPage.lat, memberHistoryPage.lon);
				BrowsePage.frame.setVisible(true);
			}
			else if(actionsource == memberSearchPage.Fifty)
			{
				time = 50;
				memberBrowsePage BrowsePage = new memberBrowsePage(50, memberHistoryPage.lat, memberHistoryPage.lon);
				BrowsePage.frame.setVisible(true);
			}
			else if(actionsource == memberSearchPage.Sixty)
			{
				time = 60;
				memberBrowsePage BrowsePage = new memberBrowsePage(60, memberHistoryPage.lat, memberHistoryPage.lon);
				BrowsePage.frame.setVisible(true);
			}
			
		}
		
	}
	
	public class MemberSearchPageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			MemberHomePage.frame.dispose();
			Object actionsource = e.getSource();
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				MemberHomePage HomePage = new MemberHomePage(memberTrack);
				HomePage.frame.setVisible(true);
			}
			else if(actioncommand.equals("Store Name"))
			{
				memberSearchPage SearchPage = new memberSearchPage();
				SearchPage.frame.setVisible(true);
			}
			else if(actioncommand.equals("Cuisine Type"))
			{
				memberSearchPage SearchPage = new memberSearchPage(1, 1);
				SearchPage.frame.setVisible(true);
			}
			else if(actioncommand.equals("Delivery Time"))
			{
				memberSearchPage SearchPage = new memberSearchPage(1, 1, 1);
				SearchPage.frame.setVisible(true);
			}
			else if(actioncommand.equals("Discount"))
			{
				memberSearchPage SearchPage = new memberSearchPage(1, 1, 1, 1);
				SearchPage.frame.setVisible(true);
			}
			else if(actioncommand.equals("Buy 1 Get 1 Free"))
			{
				memberSearchPage SearchPage = new memberSearchPage(1, 1, 1, 1, 1);
				SearchPage.frame.setVisible(true);
			}
			else if(actioncommand.equals("20 % off For All Items"))
			{
				memberSearchPage SearchPage = new memberSearchPage(1, 1, 1, 1, 1);
				SearchPage.frame.setVisible(true);
			}
			else if(which)
			{
				categoryData = null;
				try {
					categoryData = new categoryInfo();
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				for(int i = 0; i <= categoryData.getCategory().length; i++)
				{
					if(actioncommand.equals(categoryData.getCategory(i).toString()))
					{
						memberBrowsePage GoTo = new memberBrowsePage(i);
						GoTo.frame.setVisible(true);
					}
				}
			}
			else
			{
				store = null;
				try {
					store = new storeInfoAll();
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try
				{
					for(int i = 0; i <= store.getName().length; i++)
					{	
						if(SearchBox.getText().equals(store.getName(i).toString()))
						{
							OrderPage order = new OrderPage(i);
							order.frame.setVisible(true);
							break;
						}
						else if(i > store.getName().length)
						{
							throw new Exception("WrongName");
						}
					}
				}
				catch(Exception ee)
				{
					memberSearchPage NEW = new memberSearchPage(1);
					NEW.frame.setVisible(true);
				}
			}
		}

	}
}
