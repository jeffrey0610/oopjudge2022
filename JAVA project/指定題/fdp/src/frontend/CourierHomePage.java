package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

public class CourierHomePage extends Josephood{
	
	//see who login
	public static int courierTrack;
	
	public CourierHomePage()
	{
		try {
			initialization();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public CourierHomePage(int a)
	{
		courierTrack = a;
		try {
			initialization();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void initialization() throws FileNotFoundException, IOException, ParseException
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
		CourierHomePageListener CourierHomePageNextListener = new CourierHomePageListener();
		
		JPanel courierHomePanel = new JPanel();
		courierHomePanel.setBackground(new Color(70, 70 , 70));
		frame.add(courierHomePanel);
		courierHomePanel.setLayout(springLayout);	
		FB = frame.getContentPane();
		
		int dis = 347;
		JButton storeProfileButton = new JButton("Profile");
		storeProfileButton.addActionListener(CourierHomePageNextListener);
		courierHomePanel.add(storeProfileButton);
		springLayout.putConstraint(SpringLayout.WEST, storeProfileButton, dis, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeProfileButton, 410, SpringLayout.NORTH, FB);
		
		JButton storeOrdersButton = new JButton("Orders");
		storeOrdersButton.addActionListener(CourierHomePageNextListener);
		courierHomePanel.add(storeOrdersButton);
		springLayout.putConstraint(SpringLayout.WEST, storeOrdersButton, dis + 200, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeOrdersButton, 410, SpringLayout.NORTH, FB);
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(CourierHomePageNextListener);
		courierHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
		
		ImageIcon icon = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/zenrrrcvs8oln78nyjr2cxbdkajgozo6npguc59z8ydc07pmin5exjwbafx4ojcd--removebg-preview-modified.png");
		Image transform = icon.getImage();
		Image StoreImage = transform.getScaledInstance(150, 150, java.awt.Image.SCALE_SMOOTH);
		ImageIcon StoreIcon = new ImageIcon(StoreImage);
		JLabel storeicon = new JLabel(StoreIcon);
		courierHomePanel.add(storeicon);
		springLayout.putConstraint(SpringLayout.WEST, storeicon, 402, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, storeicon, 140, SpringLayout.NORTH, FB);
		
		ImageIcon gicon = new ImageIcon("C:/Users/jeffr/eclipse-workspace/fdp/assets/light-gray-circle-icon-926957.png");
		Image gtransform = gicon.getImage();
		Image gStoreImage = gtransform.getScaledInstance(158, 158, java.awt.Image.SCALE_SMOOTH);
		ImageIcon gStoreIcon = new ImageIcon(gStoreImage);
		JLabel gstoreicon = new JLabel(gStoreIcon);
		courierHomePanel.add(gstoreicon);
		springLayout.putConstraint(SpringLayout.WEST, gstoreicon, 398, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, gstoreicon, 136, SpringLayout.NORTH, FB);
		
		JLabel food = new JLabel("Your Best Is Our Best");
		courierHomePanel.add(food);
		food.setFont(new Font("Serif", Font.CENTER_BASELINE, 25));
		food.setForeground(new Color(206, 207, 130));
		springLayout.putConstraint(SpringLayout.WEST, food, 363, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, food, 320, SpringLayout.NORTH, FB);
	}
	
	public class CourierHomePageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			CourierHomePage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Profile"))
			{
				courierProfilePage CPP = new courierProfilePage();
				CPP.frame.setVisible(true);
			}
			else if(actioncommand.equals("Orders"))
			{
				if(me.checkOrder(conn) != null)
				{
					//pass the current order
					courierOrdersPage COP = new courierOrdersPage(me.checkOrder(conn));
					COP.frame.setVisible(true);
				}
				else
				{
					courierOrdersPage COP = new courierOrdersPage();
					COP.frame.setVisible(true);
				}
			}
			else if(actioncommand.equals("Back"))
			{
				Josephood HomePage = new Josephood();
				HomePage.frame.setVisible(true);
			}
			
		}

	}
}

