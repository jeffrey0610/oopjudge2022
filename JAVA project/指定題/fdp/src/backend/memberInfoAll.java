package backend;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.parser.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class memberInfoAll {

		private String[] Name;
		private String[] Address;
		private String[] Latitude;
		private String[] Longitude;
		private String[] Phone;
		private String[] Email;
		private String[] Membership;
		private String[] Password;	
		
	//import member information from json
	public memberInfoAll() throws FileNotFoundException, IOException, ParseException
	{
		
        JSONParser parser = new JSONParser();
        JSONArray wholeArray = (JSONArray) parser.parse(new FileReader("C:/Users/jeffr/eclipse-workspace/fdp/assets/members_detail.json"));
        
        this.Name = new String[wholeArray.size()];
        this.Address = new String[wholeArray.size()];
        this.Latitude = new String[wholeArray.size()];
        this.Longitude = new String[wholeArray.size()];
        this.Phone = new String[wholeArray.size()];
        this.Email = new String[wholeArray.size()];
        this.Membership = new String[wholeArray.size()];
        this.Password = new String[wholeArray.size()];
        
      	for (int i = 0; i < wholeArray.size(); i++) {
            JSONObject jsonObject = (JSONObject) wholeArray.get(i);
            
            //name
            Name[i] = jsonObject.get("name").toString();
            JSONObject positionObject = (JSONObject) jsonObject.get("position");
            Address[i] = positionObject.get("address").toString();
            Latitude[i] = positionObject.get("latitude").toString();
            Longitude[i] = positionObject.get("longitude").toString();

            //phone, email, membership
            Phone[i] = jsonObject.get("phone").toString();
            Email[i] = jsonObject.get("email").toString();
            Membership[i] = jsonObject.get("membership").toString();

            //password
            Password[i] = jsonObject.get("password").toString();
      	}
	}
	
	//getters
	public String[] getPassword() {
		return Password;
	}
	
	public String getPassword(int a) {
		return Password[a];
	}
	
	public String getName(int a) {
		return Name[a];
	}

	public String getAddress(int a) {
		return Address[a];
	}

	public String getLatitude(int a) {
		return Latitude[a];
	}

	public String getLongitude(int a) {
		return Longitude[a];
	}

	public String getPhone(int a) {
		return Phone[a];
	}

	public String getEmail(int a) {
		return Email[a];
	}

	public String getMembership(int a) {
		return Membership[a];
	}
	
	//getters
	public String[] getName() {
		return Name;
	}

	public String[] getAddress() {
		return Address;
	}

	public String[] getLatitude() {
		return Latitude;
	}

	public String[] getLongitude() {
		return Longitude;
	}

	public String[] getPhone() {
		return Phone;
	}

	public String[] getEmail() {
		return Email;
	}

	public String[] getMembership() {
		return Membership;
	}
}
