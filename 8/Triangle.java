
/**
 * Triangle 繼承 Shape class
 * 
 * @author jeffr
 *
 */
public class Triangle extends Shape {
	/**
	 * 設定Triangle的邊長初始值
	 * @param length
	 * Triangle的邊長
	 */
	public Triangle(double length) {
		super(length);
	}
	/**
	 * 重新設定Triangle的邊長
	 */
	public void setLength(double length) {
		this.length=length;
	}
	/**
	 * Triangle的面積
	 * @return
	 * 回傳Triangle的面積 = length*length*Math.sqrt(3)/4
	 */
	public double getArea() {
		double area = length*length*Math.sqrt(3)/4;
		return (Math.round(area*100)/100.0);
	}
	/**
	 * Triangle的周長
	 * @return
	 * 回傳Triangle的周長 = 3*length
	 */
	public double getPerimeter() {
		return 3*length;
	}
}
