package backend;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.parser.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class storeInfoAll {

	private String[] Name;
	private String[] Address;
	private String[] Latitude;
	private String[] Longitude;
	private String[] Phone;
	private String[] StoreDescription;
	private String[] OrderDescription;
	private String[][] MenuName;
	private String[][] MenuPrice;
	private String[][][] BusinessTime;
	private String[][] type;
	private String[] password;
	private int[] menusize;
	private int[] typesize;

	private int MenuSize = 30;

	public storeInfoAll() throws FileNotFoundException, IOException, ParseException {
		//this class is used to import json file into array.then create getters
		
		//read and parse json file
		JSONParser parser = new JSONParser();
		JSONArray wholeArray = (JSONArray) parser
				.parse(new FileReader("C:/Users/jeffr/eclipse-workspace/fdp/assets/stores_detail.json"));

		//array instance initialization
		this.Name = new String[wholeArray.size()];
		this.Address = new String[wholeArray.size()];
		this.Latitude = new String[wholeArray.size()];
		this.Longitude = new String[wholeArray.size()];
		this.Phone = new String[wholeArray.size()];
		this.StoreDescription = new String[wholeArray.size()];
		this.OrderDescription = new String[wholeArray.size()];
		this.MenuName = new String[wholeArray.size()][MenuSize];
		this.MenuPrice = new String[wholeArray.size()][MenuSize];
		this.BusinessTime = new String[wholeArray.size()][7][2];
		this.type = new String[wholeArray.size()][MenuSize];
		this.password = new String[wholeArray.size()];

		//import by each object(store), become store[i]
		for (int i = 0; i < wholeArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) wholeArray.get(i);

			// name
			Name[i] = jsonObject.get("name").toString();
			JSONObject positionObject = (JSONObject) jsonObject.get("position");
			Address[i] = positionObject.get("address").toString();
			Latitude[i] = positionObject.get("latitude").toString();
			Longitude[i] = positionObject.get("longitude").toString();

			// phone, descriptions
			Phone[i] = jsonObject.get("phone").toString();
			StoreDescription[i] = jsonObject.get("store_description").toString();
			OrderDescription[i] = jsonObject.get("order_description").toString();

			// type
			JSONArray typeObject = (JSONArray) jsonObject.get("type");
			for (int c = 0; c < typeObject.size(); c++) {
				type[i][c] = (String) typeObject.get(c);
			}

			// menu
			typeObject = (JSONArray) jsonObject.get("menu");
			for (int c = 0; c < typeObject.size(); c++) {
				JSONObject menuObject = (JSONObject) typeObject.get(c);
				MenuName[i][c] = menuObject.get("name").toString();
				MenuPrice[i][c] = menuObject.get("price").toString();
			}

			// password
			password[i] = jsonObject.get("password").toString();

			// business time, BusinessTime[object][day 0 ~ 6][0 = start, 1 = end]
			JSONObject timeObject = (JSONObject) jsonObject.get("business_time");
			JSONObject ob = (JSONObject) timeObject.get("mon");
			BusinessTime[i][0][0] = ob.get("start").toString();
			BusinessTime[i][0][1] = ob.get("end").toString();

			ob = (JSONObject) timeObject.get("tue");
			BusinessTime[i][1][0] = ob.get("start").toString();
			BusinessTime[i][1][1] = ob.get("end").toString();

			ob = (JSONObject) timeObject.get("wed");
			BusinessTime[i][2][0] = ob.get("start").toString();
			BusinessTime[i][2][1] = ob.get("end").toString();

			ob = (JSONObject) timeObject.get("thu");
			BusinessTime[i][3][0] = ob.get("start").toString();
			BusinessTime[i][3][1] = ob.get("end").toString();

			ob = (JSONObject) timeObject.get("fri");
			BusinessTime[i][4][0] = ob.get("start").toString();
			BusinessTime[i][4][1] = ob.get("end").toString();

			ob = (JSONObject) timeObject.get("sat");
			BusinessTime[i][5][0] = ob.get("start").toString();
			BusinessTime[i][5][1] = ob.get("end").toString();

			ob = (JSONObject) timeObject.get("sun");
			BusinessTime[i][6][0] = ob.get("start").toString();
			BusinessTime[i][6][1] = ob.get("end").toString();
		}
	}

	public String[] getPassword() {
		return password;
	}

	public String getPassword(int a) {
		return password[a];
	}

	//get type and cop with different length of each store
	public int[] getTypeSize() throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		JSONArray wholeArray = (JSONArray) parser
				.parse(new FileReader("C:/Users/jeffr/eclipse-workspace/fdp/assets/stores_detail.json"));
		this.typesize = new int[wholeArray.size()];

		for (int i = 0; i < wholeArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) wholeArray.get(i);
			JSONArray typeObject = (JSONArray) jsonObject.get("type");
			typesize[i] = typeObject.size();
		}

		return typesize;
	}

	public String[][] getType() {
		return type;
	}

	public String getType(int a, int b) {
		return type[a][b];
	}

	public String[] getType(int a) {
		return type[a];
	}

	public String getName(int a) {
		return Name[a];
	}

	public String getAddress(int a) {
		return Address[a];
	}

	public String getLatitude(int a) {
		return Latitude[a];
	}

	public String getLongitude(int a) {
		return Longitude[a];
	}

	public String getPhone(int a) {
		return Phone[a];
	}

	public String getStoreDescription(int a) {
		return StoreDescription[a];
	}

	public String getOrderDescription(int a) {
		return OrderDescription[a];
	}

	public String getMenuName(int a, int b) {
		return MenuName[a][b];
	}

	public String getMenuPrice(int a, int b) {
		return MenuPrice[a][b];
	}

	public String getBusinessTime(int a, int b, int c) {
		return BusinessTime[a][b][c];
	}

	// getters
	public String[] getName() {
		return Name;
	}

	public String[] getAddress() {
		return Address;
	}

	public String[] getLatitude() {
		return Latitude;
	}

	public String[] getLongitude() {
		return Longitude;
	}

	public String[] getPhone() {
		return Phone;
	}

	public String[] getStoreDescription() {
		return StoreDescription;
	}

	public String[] getOrderDescription() {
		return OrderDescription;
	}

	public String[][] getMenuName() {
		return MenuName;
	}

	public String[][] getMenuPrice() {
		return MenuPrice;
	}

	public String[][][] getBusinessTime() {
		return BusinessTime;
	}

	public int[] getMenuSize() throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		JSONArray wholeArray = (JSONArray) parser
				.parse(new FileReader("C:/Users/jeffr/eclipse-workspace/fdp/assets/stores_detail.json"));
		this.menusize = new int[wholeArray.size()];

		for (int i = 0; i < wholeArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) wholeArray.get(i);
			JSONArray typeObject = (JSONArray) jsonObject.get("menu");
			menusize[i] = typeObject.size();
		}

		return menusize;
	}

	// Store 的 googleAPI 的 HTML code
	private static String[] html = {
			"<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.4116642973604!2d121.52442321473686!3d25.08792254228224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aeaff5af4673%3A0x97a43be6cf01bce!2zMTEx5Y-w5YyX5biC5aOr5p6X5Y2A5paH5p6X6LevMTEw5be3OeiZnw!5e0!3m2!1szh-TW!2stw!4v1655183651870!5m2!1szh-TW!2stw\" width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d404.40833698455845!2d121.58071250402178!3d25.043509032549927!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aba021316165%3A0x6fe4c38829859595!2z5pil5L6G6Jq15LuU6bq157ea!5e0!3m2!1szh-TW!2stw!4v1655183764011!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d228.31542077482126!2d121.54231589745017!3d25.025179487649883!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442ab5c44c81e59%3A0x5ff596fec63a4344!2zTWlzcyBFbmVyZ3kg6IO96YeP5bCP5aeQIOWPsOWMl-WSjOW5s-W6lw!5e0!3m2!1szh-TW!2stw!4v1655183879758!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.0315225912027!2d121.5295345!3d25.066920699999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a9fbd5afce25%3A0x61a3498b928ef4eb!2z5qi46aOfIFNpbXBsZSBEaWV0!5e0!3m2!1szh-TW!2stw!4v1655184095938!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.4882924491285!2d121.56479691473669!3d25.085327142386554!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442adfdd1735a2d%3A0x98220ae9ba42bf5!2z54Wy5p-P5oCd57at!5e0!3m2!1szh-TW!2stw!4v1655184169158!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.2695181701106!2d121.54314431500605!3d25.024925983976683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aa2e86274779%3A0xb00837dae135f764!2z55ub5ZGz6LGQ54Kt54Ok54eS6aSF!5e0!3m2!1szh-TW!2stw!4v1655184300164!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d225.94447180747167!2d121.55325107528016!3d25.030288212731488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abcc97bfd765%3A0xc4013ccd9fb95542!2zVUZP6LuK6Lyq6aSFLemAmuWMluW6lw!5e0!3m2!1szh-TW!2stw!4v1655184451770!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d225.85843602320617!2d121.57838354619219!3d25.076970237612112!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442ac63475baeb9%3A0x5097a49bcf517c22!2z57ev5aSn6Zue5o6S5YWn5rmW5rGf5Y2X5bqX!5e0!3m2!1szh-TW!2stw!4v1655184548610!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d225.83660113580538!2d121.5182990137071!3d25.088804668829884!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aeb714dcb0d9%3A0x28badb6e145bb0ef!2z5Y-k5pep5ZGz6IW_5bqr6aOv6LGs6IWz6aOv!5e0!3m2!1szh-TW!2stw!4v1655184785044!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=“\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d903.439886140728!2d121.57545142924543!3d25.07613779899705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442ac7b4f03386b%3A0x6546df149b5ebd31!2zMTE05Y-w5YyX5biC5YWn5rmW5Y2A5rGf5Y2X6KGXMTE56Jmf!5e0!3m2!1szh-TW!2stw!4v1655185010877!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3689.232515085901!2d121.51614648837824!3d25.091389931118133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aeb9c0d0926f%3A0xba6ea869852d6253!2z6Zi_55C06Ieq5Yqp6aSQ!5e0!3m2!1szh-TW!2stw!4v1655185050863!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d451.9178506796253!2d121.49930611904324!3d25.022437899999993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a9b65191efd5%3A0x6557c5c12fc4ff00!2zMTA45Y-w5YyX5biC6JCs6I-v5Y2A6JCs5aSn6LevMzg16Jmf!5e0!3m2!1szh-TW!2stw!4v1655185119886!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.6510298532307!2d121.57658341559583!3d25.07981438395083!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442ac60fcdafacd%3A0xacbe54bd3edab!2z6JCK5a6i5r2k6aSF5o2y!5e0!3m2!1szh-TW!2stw!4v1655185193459!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.9567237427327!2d121.61569951559586!3d25.069455883955637!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442acb089c6c8c7%3A0x52dcaa6e6cb9cee9!2z6aCC5bCW5o6S6aqo!5e0!3m2!1szh-TW!2stw!4v1655185237297!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.547876578951!2d121.56702531559543!3d25.049413183965182!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442ab976946ad3d%3A0xa78a49a2707600c7!2z5Y-w5YyX5biC5p2-5bGx5Y2A5YWr5b636Lev5Zub5q61!5e0!3m2!1szh-TW!2stw!4v1655185309465!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d903.8229230997205!2d121.5290076292454!3d25.02417319899856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a9a9d0e165f3%3A0x125ad3f861b64e40!2zVW5jbGUgTDIg5omL5L2c6IiS6IqZ6JW-55Sc6bue5bqX!5e0!3m2!1szh-TW!2stw!4v1655185357140!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d995.8249561574969!2d121.47158637939162!3d25.10748772900156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442af3eebc4cd81%3A0xd0149a5874226af6!2zMTEx5Y-w5YyX5biC5aOr5p6X5Y2A5bu25bmz5YyX6Lev5Lmd5q61!5e0!3m2!1szh-TW!2stw!4v1655185395473!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.0446256265536!2d121.54161811559528!3d25.032559583973!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442ab53fa673baf%3A0x78a1c78d3b65765f!2z5aSn5a6J56uZ6YKj6YKKIOeyvue3u-eGseeCkg!5e0!3m2!1szh-TW!2stw!4v1655185425640!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.9119803623616!2d121.57600931559539!3d25.03706098397101!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aba6927d0e4b%3A0x8ae6b62a43191ac3!2z5Y-w54Gj56ys5LiA5ZGzIFRlYSBUb3Ag5p2-5bGx5bqX!5e0!3m2!1szh-TW!2stw!4v1655185453592!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.100137960993!2d121.52267181559581!3d25.06459488395802!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a944d9e9cfa5%3A0xb6272d7ef36fdcf6!2z5Y-w54Gj5aSn5ZKW5ZOp!5e0!3m2!1szh-TW!2stw!4v1655185478281!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.43634094705!2d121.59314441559548!3d25.053195883963358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442ab6523d034c9%3A0x9c0eacc5f8aceee9!2zMTE15Y-w5YyX5biC5Y2X5riv5Y2A5ZCR6Zm96LevOTDomZ8!5e0!3m2!1szh-TW!2stw!4v1655185556869!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3615.2855836756703!2d121.52729331559517!3d25.024380583976924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a981a8377009%3A0x1ed47390bfb7eecb!2z5aO55qOu4oCi5rOw6aOf5aCCKOW4q-Wkp-esrOS4gOiFvyko5bir5aSn5aSc5biCKQ!5e0!3m2!1szh-TW!2stw!4v1655185608407!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d225.89028082047784!2d121.5467314776604!3d25.059701100000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abe50a919e09%3A0xc5cf910436e67e74!2zMTA15Y-w5YyX5biC5p2-5bGx5Y2A5rCR5qyK5p2x6Lev5LiJ5q61MTYw5be3MjXomZ8!5e0!3m2!1szh-TW!2stw!4v1655185678934!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d903.509123461482!2d121.54031972924544!3d25.066752198997357!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abde2e7ccbe5%3A0xf067ad9ae3abe35e!2z6YKx5aSq5aSq55Sc5ZOB5Z2K!5e0!3m2!1szh-TW!2stw!4v1655185725179!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=“\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d903.6245906777899!2d121.54323792924544!3d25.051092498997797!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abddebcaa089%3A0x189c7e881b611869!2z6ICB6JSh5rC054WO5YyFIOW-qeiIiOW6lw!5e0!3m2!1szh-TW!2stw!4v1655185790372!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=“\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.38763444242!2d121.50933931559574!3d25.054847583962506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a91481af83e3%3A0x3d1f8d33b74c5ef6!2z5buj5aSn6aCG6aOf5ZOB5bqX!5e0!3m2!1szh-TW!2stw!4v1655185819042!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=“\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.399070434528!2d121.57784501559553!3d25.054459783962738!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442ab2f7c2a7abb%3A0x4d52a9a5b4e44d4e!2z55yf5ZGz6IKJ5ZyT!5e0!3m2!1szh-TW!2stw!4v1655185847377!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=“\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d903.6478393516396!2d121.54875862924541!3d25.047938398997896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abe8ad07b41d%3A0xc6f4b790341dee06!2z54-N6Jyc5ZKW5ZWh5Z2KICjnj43onJzkvr_nlbYp!5e0!3m2!1szh-TW!2stw!4v1655185896994!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=“\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",
			"<iframe\r\n"
					+ "    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.1717232237825!2d121.52685851559566!3d25.062168183959177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a95b9a2f6a2f%3A0x6e7f27a58e72f37e!2zR0FCQSDlhYPmsJfjga7mupDvvIHlmI7lkKfvvIEoR0FCQeaXpeW8j-mjr-ezsOW6lyk!5e0!3m2!1szh-TW!2stw!4v1655185947404!5m2!1szh-TW!2stw\"\r\n"
					+ "    width=\"770\" height=\"450\" style=\"border:0;\" allowfullscreen=“\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>",

	};

	public static String googleAPI(int i) {

		try {
			return html[i];
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
