package backend;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.parser.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class categoryInfo {

	private String[] category;
		
	//handle type object in each store for category search
	public categoryInfo() throws FileNotFoundException, IOException, ParseException
	{
		
        JSONParser parser = new JSONParser();
        JSONArray wholeArray = (JSONArray) parser.parse(new FileReader("C:/Users/jeffr/eclipse-workspace/fdp/assets/category.json"));
                
        this.category = new String[wholeArray.size()];
        
      	for (int i = 0; i < wholeArray.size(); i++) {
      		JSONObject wholeObject = (JSONObject) wholeArray.get(i);
      		category[i] = wholeObject.get("type").toString();
      	}
	}

	public String[] getCategory() {
		return category;
	}
	
	public String getCategory(int a) {
		return category[a];
	}
	
}
