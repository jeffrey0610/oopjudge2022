
/**
 * Circle 繼承 Shape class
 * 
 * @author jeffr
 *
 */
public class Circle extends Shape {
	/**
	 * 設定Circle的邊長初始值
	 * @param length
	 * Circle的直徑
	 */
	public Circle(double length) {
		super(length);
	}
	/**
	 * 重新設定Circle的直徑
	 */
	public void setLength(double length) {
		this.length=length;
	}
	/**
	 * Circle的面積
	 * @return
	 * 回傳Circle的面積 = PI*length*length/4
	 */
	public double getArea() {
		double area = Math.PI*length*length/4;
		return (Math.round(area*100)/100.0);
	}
	/**
	 * Circle的周長
	 * @return
	 * 回傳Circle的周長 =PI*length
	 */
	public double getPerimeter() {
		return (Math.round(Math.PI*length*100)/100.0);

	}
}
