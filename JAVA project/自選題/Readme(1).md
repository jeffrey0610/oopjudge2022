# Background
利用本次期末報告的機會研究每天都會接觸的聊天軟體，將其與Java做結合。
# Import
import jframe
# Usage
> 藉由輸入ip位址進入線上聊天室，由clint端與server端進行對談，兩端可以透過文字與傳送圖片進行交談，最後可以點擊離開以來開聊天室。
# Login
>input the ip address xxx.xx.xxx.xx ex:114.37.190.38 to enter the conversation
# Maintainers
b09505002 葉承瀚
b09505026 朱睿麒

#send_msg
## 參數
 run
 
 Socket
 
 InetSocketAddress
 
 BufferedOutputStream
 
 Client

# recv_msg
## 參數
 run
 
 Socket
 
 len
 
 BufferedOutputStream
 
 Server

# send_pic 
## 參數
  run
 
 Socket
 
 InetSocketAddress
 
 BufferedOutputStream
 
 FileInputStream
 
 len
 
 
  
# recv_pic 
## 參數
  run
 
 Socket
 
 BufferedOutputStream
 
 ByteArrayOutputStream
 
 
