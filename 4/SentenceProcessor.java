/**
 * 
 * @author 高孝傑
 *
 */
public class SentenceProcessor {
	/**
	 * 刪除重複的單字
	 * 將sentence分開後存入s array裡面
	 * 選定一單字與茜面單字比對，如一樣則刪除
	 * @param sentence
	 * @return output
	 */
	public String removeDuplicatedWords(String sentence) {
		String s[];
		String output="";
		s=sentence.split(" ");
		for(int i=0 ; i<s.length ; i++) {
			for(int j = i+1 ; j<s.length ; j++) {
				if(s[i].equals(s[j])) s[j]="cut";
			}
		}
		for(int i=0 ; i<s.length ; i++) {
			if (s[i]!="cut") {
				output += s[i] + " " ;
			}
		}
		output=output.substring(0,output.length()-1);
		return output;
	}
	/**
	 * 將sentence分開後存入s array裡面
	 * 尋找與target一樣的單字，並用replacement取代
	 * @param target 
	 * 被取代的單字
	 * @param replacement
	 * 替換成的單字
	 * @param sentence
	 * @return output
	 */
	public String replaceWord(String target,String replacement, String sentence) {
		
		String s[];
		String output="";
		s=sentence.split(" ");
		for(int i=0 ; i<s.length ; i++) {
			if(s[i].equals(target)) s[i]=replacement;
		}
		for(int i=0 ; i<s.length ; i++) {
			output += s[i] + " " ;
		}
		output=output.substring(0,output.length()-1);
		return output;
	}
	
}
