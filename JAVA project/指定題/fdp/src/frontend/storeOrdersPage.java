package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

import backend.Order;
import backend.storeInfoAll;
import backend.Order.Status;
import frontend.memberTrackPage.StoreOrdersPageListener;

public class storeOrdersPage extends StoreHomePage {

	public static Order[] NewOrder;
	public static int OrderNumber = 0;
	public storeInfoAll store;
	public static JPanel storeHomePanel = new JPanel();
	public JLabel[] selectItem;
	public JLabel[] selectPrice;
	public JLabel[] selectNumber;
	public JLabel[] selectSum;
	public int[] individulSum;
	public String[] SumString;
	public StoreOrdersPageListener StoreOrdersPageNextListener;

	public static Order thisSorder;
	
	public storeOrdersPage(Order order)
	{
		if(!order.storeReject)
		{
			thisSorder = order;
			initialization();
			Display(order);
		}
		else
		{
			initialization();
			Display();
		}
	}
	
	private void Display() {
		// TODO Auto-generated method stub
		StoreOrdersPageNextListener = new StoreOrdersPageListener();
		storeHomePanel = new JPanel();
		storeHomePanel.setBackground(new Color(70, 70 , 70));
		storeHomePanel.setLayout(springLayout);	
		frame.add(storeHomePanel);
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(StoreOrdersPageNextListener);
		storeHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
	}

	public storeOrdersPage(int xStore)
	{
		initialization();
		Display(xStore);
	}
	
	private void Display(Order order) {
		
		thisSorder = order;
		StoreOrdersPageNextListener = new StoreOrdersPageListener();
		storeHomePanel = new JPanel();
		storeHomePanel.setBackground(new Color(70, 70 , 70));
		storeHomePanel.setLayout(springLayout);	
		frame.add(storeHomePanel);
		
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(StoreOrdersPageNextListener);
		storeHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, storeHomePanel);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, storeHomePanel);
		
		//set and vanish confirm/reject buttons
		if(me.checkOrder(conn) != null)
		{
			JButton confirmButton = new JButton("Confirm");
			confirmButton.addActionListener(StoreOrdersPageNextListener);
			storeHomePanel.add(confirmButton);
			springLayout.putConstraint(SpringLayout.WEST, confirmButton, 863, SpringLayout.WEST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, confirmButton, 400, SpringLayout.NORTH, FB);
			
			JButton rejectButton = new JButton("Reject");
			rejectButton.addActionListener(StoreOrdersPageNextListener);
			storeHomePanel.add(rejectButton);
			springLayout.putConstraint(SpringLayout.WEST, rejectButton, 869, SpringLayout.WEST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, rejectButton, 450, SpringLayout.NORTH, FB);
		}
		
		//set alignment
		int BorderGap = 70;
		int BorderMultiplier = 35;
		
		//initialization
		selectItem = new JLabel[order.mealsize];
		selectPrice = new JLabel[order.mealsize];
		selectNumber = new JLabel[order.mealsize];
		selectSum = new JLabel[order.mealsize];
		individulSum = new int[order.mealsize];
		SumString = new String[order.mealsize];
		
		//show current order
		for(int j = 0; j < order.mealsize; j++)
		{
			selectItem[j] = new JLabel(order.selectedItems[j]);
			storeHomePanel.add(selectItem[j]);
			selectItem[j].setForeground(Color.YELLOW);
			springLayout.putConstraint(SpringLayout.WEST, selectItem[j], 10, SpringLayout.EAST, FB);
			springLayout.putConstraint(SpringLayout.NORTH, selectItem[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);

			selectPrice[j] = new JLabel(order.selectedPrice[j] + " X ");
			storeHomePanel.add(selectPrice[j]);
			selectPrice[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectPrice[j], 10, SpringLayout.EAST, selectItem[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectPrice[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
			
			String number = String.valueOf(order.selectedNumber[j] + " = ");
			selectNumber[j] = new JLabel(number);
			storeHomePanel.add(selectNumber[j]);
			selectNumber[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectNumber[j], 10, SpringLayout.EAST, selectPrice[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectNumber[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
			
			int price = Integer.parseInt(order.selectedPrice[j]);
			individulSum[j] = price * order.selectedNumber[j];
			SumString[j] = String.valueOf(individulSum[j]);
			selectSum[j] = new JLabel(SumString[j]);
			storeHomePanel.add(selectSum[j]);
			selectSum[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, selectSum[j], 10, SpringLayout.EAST, selectNumber[j]);
			springLayout.putConstraint(SpringLayout.NORTH, selectSum[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, FB);
		}
		
	}
	
	private void Display(int xStore) {
		
		StoreOrdersPageNextListener = new StoreOrdersPageListener();
		storeHomePanel = new JPanel();
		storeHomePanel.setBackground(new Color(70, 70 , 70));
		frame.add(storeHomePanel);
		storeHomePanel.setLayout(springLayout);		
		FB = frame.getContentPane();
		
		JButton backButton = new JButton("Back");
		backButton.addActionListener(StoreOrdersPageNextListener);
		storeHomePanel.add(backButton);
		springLayout.putConstraint(SpringLayout.WEST, backButton, 880, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, backButton, 500, SpringLayout.NORTH, FB);
	}

	public storeOrdersPage()
	{
		initialization();
	}

	public void initialization()
	{
		
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class StoreOrdersPageListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			storeOrdersPage.frame.dispose();
			
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back"))
			{
				StoreHomePage HomePage = new StoreHomePage(xStore);
				HomePage.frame.setVisible(true);
			}
			else if(actioncommand.equals("Confirm"))
			{
				thisSorder.storeConfirm = true;
				storeOrdersPage confirmedPage = new storeOrdersPage(thisSorder);
				confirmedPage.frame.setVisible(true);
				try {
					thisSorder.changeOrderStatus(thisSorder.orderID, conn, Status.Order_is_being_prepared.toString());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			else if(actioncommand.equals("Reject"))
			{
				thisSorder.storeReject = true;
				thisSorder.courierReject = true;
				storeOrdersPage rejectedPage = new storeOrdersPage(thisSorder);
				rejectedPage.frame.setVisible(true);
				try {
					thisSorder.changeOrderStatus(thisSorder.orderID, conn, Status.Order_failed.toString());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
	}
}
