package frontend;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import org.json.simple.parser.ParseException;

import backend.Order;
import backend.Store;
import backend.storeInfoAll;

public class OrderPage extends Josephood{
	
	public static JButton[] addButton;
	public JTextField[] NumberOfProducts;
	public static JButton[] subtractButton;
	public JLabel[] item;
	public JLabel[] itemPrice;
	private int BoxWidth = 3;
	public JLabel StoreName;
	public static int[] numberOfItems;
	public String number;
	public static int[] menuSize;
	public static int passedI;
	public static int sum = 0;
	public storeInfoAll store;
	public static JLabel showSum;
	public String orderID;
	
	public OrderPage(int xStore)
	{
		try {
			initialization();
			showStoreMenu(xStore);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//show all items of the order
	public void showStoreMenu(int i) throws FileNotFoundException, IOException, ParseException
	{
		
		passedI = i;
		PlaceOrderListener PlaceOrderNextListener = new PlaceOrderListener();
		PlaceOrderCalListener PlaceOrderCalNextListener = new PlaceOrderCalListener();
		store = new storeInfoAll();
		SpringLayout springLayout = new SpringLayout();
		
		JPanel orderHomePanel = new JPanel();
		orderHomePanel.setBackground(new Color(70, 70 , 70));
		frame.add(orderHomePanel);
		orderHomePanel.setLayout(springLayout);	
		
		menuSize = store.getMenuSize();
		int BorderGap = 70;
		int BorderMultiplier = 35;
		
		this.addButton = new JButton[menuSize[i]];
		this.NumberOfProducts = new JTextField[menuSize[i]];
		this.subtractButton = new JButton[menuSize[i]];
		this.item = new JLabel[menuSize[i]];
		this.itemPrice = new JLabel[menuSize[i]];
		
		Container FB = frame.getContentPane();
		
		StoreName = new JLabel(store.getName(i));
		orderHomePanel.add(StoreName);
		StoreName.setForeground(Color.WHITE);
		StoreName.setFont(new Font("Serif", Font.PLAIN, 25));
		springLayout.putConstraint(SpringLayout.WEST, StoreName, 20, SpringLayout.EAST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, StoreName, 17, SpringLayout.NORTH, FB);
		numberOfItems = new int[menuSize[i]];
		
		for(int j = 0; j < menuSize[i]; j++)
		{
			numberOfItems[j] = 0;
			addButton[j] = new JButton("+");
			addButton[j].addActionListener(PlaceOrderCalNextListener);
			orderHomePanel.add(addButton[j]);
			addButton[j].setBackground(Color.LIGHT_GRAY);
			springLayout.putConstraint(SpringLayout.WEST, addButton[j], 20, SpringLayout.WEST, orderHomePanel);
			springLayout.putConstraint(SpringLayout.NORTH, addButton[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, orderHomePanel);
			
			number = String.valueOf(numberOfItems[j]);
			this.NumberOfProducts[j] = new JTextField(number, BoxWidth);
			orderHomePanel.add(NumberOfProducts[j]);
			NumberOfProducts[j].setBackground(Color.WHITE);
			springLayout.putConstraint(SpringLayout.WEST, NumberOfProducts[j], 2, SpringLayout.EAST, addButton[j]);
			springLayout.putConstraint(SpringLayout.NORTH, NumberOfProducts[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, orderHomePanel);
			
			subtractButton[j] = new JButton("-");
			subtractButton[j].addActionListener(PlaceOrderCalNextListener);
			orderHomePanel.add(subtractButton[j]);
			subtractButton[j].setBackground(Color.LIGHT_GRAY);
			springLayout.putConstraint(SpringLayout.WEST, subtractButton[j], 2, SpringLayout.EAST, NumberOfProducts[j]);
			springLayout.putConstraint(SpringLayout.NORTH, subtractButton[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, orderHomePanel);
			
			item[j] = new JLabel(store.getMenuName(i, j));
			orderHomePanel.add(item[j]);
			item[j].setForeground(Color.YELLOW);
			springLayout.putConstraint(SpringLayout.WEST, item[j], 10, SpringLayout.EAST, subtractButton[j]);
			springLayout.putConstraint(SpringLayout.NORTH, item[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, orderHomePanel);

			itemPrice[j] = new JLabel(store.getMenuPrice(i, j) + " NTD");
			orderHomePanel.add(itemPrice[j]);
			itemPrice[j].setForeground(new Color(230, 240, 125));
			springLayout.putConstraint(SpringLayout.WEST, itemPrice[j], 10, SpringLayout.EAST, item[j]);
			springLayout.putConstraint(SpringLayout.NORTH, itemPrice[j], BorderMultiplier*j + BorderGap, SpringLayout.NORTH, orderHomePanel);
			
		}
		
		JButton PlaceOrderButton = new JButton("Place Order");
		PlaceOrderButton.addActionListener(PlaceOrderNextListener);
		orderHomePanel.add(PlaceOrderButton);
		springLayout.putConstraint(SpringLayout.WEST, PlaceOrderButton, 850, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, PlaceOrderButton, 500, SpringLayout.NORTH, FB);
		
		JButton BackToBrowse = new JButton("Back to Browse");
		BackToBrowse.addActionListener(PlaceOrderNextListener);
		orderHomePanel.add(BackToBrowse);
		springLayout.putConstraint(SpringLayout.WEST, BackToBrowse, 825, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, BackToBrowse, 30, SpringLayout.NORTH, FB);
		
		JButton BackToSearch = new JButton("Back to Search");
		BackToSearch.addActionListener(PlaceOrderNextListener);
		orderHomePanel.add(BackToSearch);
		springLayout.putConstraint(SpringLayout.WEST, BackToSearch, 827, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, BackToSearch, 30, SpringLayout.NORTH, BackToBrowse);
		
		showSum = new JLabel("Total = " + sum + " NTD");
		orderHomePanel.add(showSum);
		showSum.setForeground(Color.WHITE);
		showSum.setFont(new Font("Serif", Font.TRUETYPE_FONT, 18));
		springLayout.putConstraint(SpringLayout.WEST, showSum, 650, SpringLayout.WEST, FB);
		springLayout.putConstraint(SpringLayout.NORTH, showSum, 500, SpringLayout.NORTH, FB);
		
	}
	
	public void initialization() throws FileNotFoundException, IOException, ParseException
	{
		frame = new JFrame();
		frame.setSize(WIDTH, HEIGHT);
		frame.setLocationRelativeTo(null);
	}
	
	public class PlaceOrderListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			OrderPage.frame.dispose();
			String actioncommand = e.getActionCommand();
			
			if(actioncommand.equals("Back to Browse"))
			{
				memberBrowsePage Browse = new memberBrowsePage();
				Browse.frame.setVisible(true);
			}
			else if(actioncommand.equals("Back to Search"))
			{
				memberSearchPage Search = new memberSearchPage();
				Search.frame.setVisible(true);
			}
			else if(actioncommand.equals("Place Order"))
			{
				Order order = new Order();
				order.price = 0;
				order.price = sum;
				order.courierConfirm = false;
				order.courierReject = false;
				order.storeConfirm = false;
				order.storeReject = false;

				//when placing order, passedI and xStore identify at which store the order is placed
				
				for(int k = 0; k < OrderPage.menuSize[OrderPage.passedI]; k++)
				{
					if(OrderPage.numberOfItems[k] != 0)
					{
						order.mealsize++;
					}
				}		
				
				//initialize information of the order
				order.selectedItems = new String[order.mealsize];
				order.selectedPrice = new String[order.mealsize];
				order.selectedNumber = new int[order.mealsize];
				int count = 0;
				String info = null;
				for(int tracker = 0; tracker < OrderPage.menuSize[OrderPage.passedI]; tracker++)
				{
					if(OrderPage.numberOfItems[tracker] != 0)
					{
						order.selectedItems[count] = store.getMenuName(passedI, tracker);
						order.selectedPrice[count] = store.getMenuPrice(passedI, tracker);
						order.selectedNumber[count] = OrderPage.numberOfItems[tracker];
						count++;
						info += store.getMenuName(passedI, tracker);
					}
				}		
				
				order.xStore = passedI;
				
				//after placing order, go to Track page
				memberTrackPage Track = new memberTrackPage(order);
				Track.frame.setVisible(true);
				
				//store the order into jdbc database for implementation of history function and pass the Order to both Store and Courier
				Store thisstore = new Store(store.getName(passedI));
				order.Info = info;
				me.order(conn, sum, thisstore, order);
				
				//reset sum
				sum = 0;
				
			}
			
		}

	}
	
	public class PlaceOrderCalListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
		
			String actioncommand = e.getActionCommand();
			Object actionsource = e.getSource();
			
			//calculation  of sum and use buttons to help simplify the process of placing order.
			//tracker is used to trace down at which store the order is being placed.
			for(int tracker = 0; tracker < OrderPage.menuSize[OrderPage.passedI]; tracker++)
				if(actioncommand.equals("+") && actionsource == OrderPage.addButton[tracker])
				{
					OrderPage.numberOfItems[tracker]++;
					number = String.valueOf(OrderPage.numberOfItems[tracker]);
					NumberOfProducts[tracker].setText(number);
					sum += Integer.parseInt(store.getMenuPrice(OrderPage.passedI, tracker));
					showSum.setText("Total = " + sum + " NTD");
				}
				else if(actioncommand.equals("-") && actionsource == OrderPage.subtractButton[tracker])
				{
					if(OrderPage.numberOfItems[tracker] > 0)
					{
						OrderPage.numberOfItems[tracker]--;
						number = String.valueOf(OrderPage.numberOfItems[tracker]);
						NumberOfProducts[tracker].setText(number);
						sum -= Integer.parseInt(store.getMenuPrice(OrderPage.passedI, tracker));
						showSum.setText("Total = " + sum + " NTD");
					}
					else if(!(OrderPage.numberOfItems[tracker] > 0))
					{
						OrderPage.numberOfItems[tracker] = 0;
						NumberOfProducts[tracker].setText("0");
					}
				}
			
			
			
		}

	}

}
