
/**
 * A command is composed of “operator” and “value” separated by a space. There
 * are five kinds of unknown command exceptions: First, the operator is any
 * operator symbol other than +, -, *, or /. Second, the value can’t be parsed
 * as a real number. Third, the operator and the value are both invalid. Fourth,
 * the command is not formed by exact one operator and one value separated by
 * one space. Last, the command indicates dividing by zero. An
 * UnknownCmdException will be thrown if one of these exceptions described above
 * happens. You need to pass the error message to UnknownCmdException class in
 * order to figure out what happens. The system will call exception.getMessage()
 * to check it out.
 * 
 */

public class UnknownCmdException extends Exception {

    /**
     * 1. invalid operator: "[operator] is an unknown operator" 2. invalid value:
     * "[value] is an unknown value" 3. both invalid: "[operator] is an unknown
     * operator and [value] is an unknown value" 4. invalid form: "Please enter 1
     * operator and 1 value separated by 1 space" 5. dividing by zero: "Can
     * notdivide by 0"
     */

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param errMessage is among five probable error messages mentioned above
     */
    public UnknownCmdException(String errMessage) {
        super(errMessage);
    }
}