package backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import backend.Order.Status;
import frontend.CourierInfo;

public class Courier {
//	Courier參數
	public String name;
	public String account;
	private String password;
	public String phoneNumber;
	public String payDate;
	public String email;
//	確認訂單狀態
	public boolean acception;
	public boolean pickup;
	public boolean dropoff;
	public boolean delivered;



//	將Courier的資訊放進SQLite
	public void save(Connection conn) {
		try {
			String sql = "INSERT INTO courier (name, account,password,phoneNumber,email) VALUES (?,?,?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, this.name);
			pstmt.setString(2, this.account);
			pstmt.setString(3, this.password);
			pstmt.setString(4, this.phoneNumber);
			pstmt.setString(5, this.email);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}

	}
// 	是否接單(依照距離計算)
	public boolean acceptOrder(Order order, Store store, Consumer consumer) {
		double distance = getDistance(Double.parseDouble(store.latitude), Double.parseDouble(store.longitude), consumer.lat, consumer.lng);
		if (distance >= 500) {
			acception = false;
			return false;
		} else {
			acception = true;
			return true;
		}

	}

// 	get distance
	public double getDistance(double lat_s, double lng_s, double lat_c, double lng_c) {
		double lat = rad(lat_s) - rad(lat_c);
		double lng = rad(lng_s) - rad(lng_s);
		double dis = (float) (2 * Math.asin(Math.sqrt(Math.pow(Math.sin(lat / 2), 2)
				+ Math.cos(rad(lat_s)) * Math.cos(rad(lat_c)) * Math.pow(Math.sin(lng / 2), 2))));
		dis = Math.round(dis * 6378137 * 10000) / 10000;
		return dis;
	}

	private double rad(double d) {
		return d * Math.PI / 180.0;
	}

	// 確認訂單狀態
	public String orderStatus(ArrayList<Order> orderDB) {
		if (acception = true) {
			if (pickup = true) {
				if (dropoff = true) {
					Status D = Status.Order_is_arrived;
					return D.toString();
				} else {
					Status C = Status.Order_is_on_the_way;
					return C.toString();
				}
			} else {
				Status B = Status.Order_is_being_prepared;
				return B.toString();
			}
		} else {
			Status A = Status.Confirming_order;
			return A.toString();
		}

	}

	// 訂單是否完成
	public boolean orderDelivery(Order order) {

		if (acception == true && pickup == true && dropoff == true) {
			delivered = true;
			CourierInfo.pastDeliveries.add(order);
			return true;
		} else {
			delivered = false;
			return false;
		}

	}

//外送員歷史訂單
	public ArrayList<Order> getHistoryDelivery(Connection conn) throws SQLException {
		String query = "select * from 'order' where deliveryPersonName=?";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, this.name);
		ResultSet rs = statement.executeQuery();

		ArrayList<Order> historyOrder = new ArrayList<Order>();
		while (rs.next()) {

			Order o = new Order();
			o.orderID = rs.getInt("orderID");
			o.orderStatus = rs.getString("orderStatus");
			o.price = rs.getDouble("price");
			o.timeOfOrderCreated = rs.getDate("timeOfOrderCreated");
			o.timeOfArrival = rs.getDate("timeOfArrival");
			o.deliveryLocation = rs.getString("deliveryLocation");
			o.consumerName = rs.getString("consumerName");
			o.deliveryPersonName = rs.getString("deliveryPersonName");
			o.storename = rs.getString("storename");
			o.Info = rs.getString("Info");

			historyOrder.add(o);
		}
		return historyOrder;
	}

}